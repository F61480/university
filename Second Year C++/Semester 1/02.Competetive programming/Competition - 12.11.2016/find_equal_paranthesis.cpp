#include <iostream>
#include <stack>
    
using namespace std;

int main(){
    string entry;
    
    while(cin >> entry) {
        bool stop = true;
        
        stack<char> tmp;
        
        for(int i = 0;i < entry.size(); i++){
            if(entry[i]=='{' || entry[i]=='[' || entry[i]=='(' ){
                tmp.push(entry[i]);
            }
            
            else if(!tmp.empty() && ((tmp.top() == '{' && entry[i] == '}') || (tmp.top() == '[' && entry[i] == ']') || (tmp.top() == '(' && entry[i] == ')')))
                tmp.pop();
                else {
                    stop = false;
                    break;
                }
        }
        
        if(stop == false || !tmp.empty()) {
            cout<<"no"<< endl;
        }
        else {
            cout<<"yes"<< endl;
        }
    }
    
    return 0;
    }
