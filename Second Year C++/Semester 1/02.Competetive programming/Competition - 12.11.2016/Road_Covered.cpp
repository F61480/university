#include <iostream>
#include <string>
#include <vector>


using namespace std;

int main() {
    
    int n;
    
    while(cin >> n) {
    
    if(n < 0) {
        return 1;
    }
    
    vector<int> speed;
    vector<int> time;
    
    int results = 0;
    int new_time = 0;
    
    for(int i = 0; i < n; i++) {
        int a, b;
        cin >> a >> b;
        
        speed.push_back(a);
        time.push_back(b);
    }
    
    for(int i = 0; i < speed.size(); i++) {
        results += speed[i] * time[i];
        for(int j = i + 1; j < speed.size(); j++) {
            new_time = time[j] - time[i];
            time[j] = new_time;
        }
    }
    
    cout << results << " miles" << endl;
    }
    
    return 0;
}
