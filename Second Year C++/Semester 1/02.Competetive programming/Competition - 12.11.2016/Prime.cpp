#include <iostream>
#include <string>


using namespace std;

int main() {
    
    int try_count;
    cin >> try_count;
    
    for(int k = 0; k < try_count; k++) {
        long long a, b;
        cin >> a >> b;
        int counter = 0;
        
        for(int i = a; i <= b; i++) {
            for(int j = 2; j <= i; j++) {
                if(!(i%j) && (i!=j)) {
                    break;
                }
                if(j == i) {
                    counter++;
                }
            }
        }
        cout << counter << endl;
    }
    
    return 0;
}
