//13-NOV-2016
//Tihomir Mladenov, F-61480
//Dequeue + CircularArr

#include <iostream>
#include <string>

using namespace std;

#define SIZE 256

template <typename T>
class CDequeue {
private:
    int first;
    int last;
    int sizes;
    T* arr;
public:
    CDequeue(int s) {
        first = 1;
        last = 0;
        sizes = 0;
        arr = new T[s];
    }
    
    int size() {
        return sizes;
    }
    
    bool isEmpty() {
        return (sizes == 0);
    }
    
    virtual T getFirst() {
        if(isEmpty()) {
            throw string("Dequeue is Empty.\n");
        }
        
        return arr[(first - 1) % SIZE];
    }
    
    virtual T getLast() {
        if(isEmpty()) {
            throw string("Dequeue is Empty.\n");
        }
        
        return arr[(last + 1) % SIZE];
    }
    
    virtual void insertFirst(T stuff) {
        if(sizes == SIZE) {
            throw string("Dequeue overflow.\n");
        }
        arr[first] = stuff;
        first = (first + 1) % SIZE;
        sizes++;
    }
    
    virtual T removeFirst() {
        if(isEmpty()) {
            throw string("Dequeue is Empty.\n");
        }
        first = (first - 1) % SIZE;
        sizes--;
        return arr[first];
    }
    
    virtual void insertLast(T Stuff) {
        if(sizes == SIZE) {
            throw string("Dequeue overflow.\n");
        }
        
        arr[last] = Stuff;
        last = (last - 1) % SIZE;
        sizes++;
    }
    
    virtual T removeLast() {
        if(isEmpty()) {
            throw string("Dequeue is empty.\n");
        }
        
        last = (last + 1) % SIZE;
        sizes--;
        return arr[last];
    }
    
    ~CDequeue() {
        delete [] arr;
        
        first = 1;
        last = 0;
        sizes = 0;
    }
    
};


int main() {
    CDequeue<string> a(2);
    a.insertFirst("AF");
    a.insertLast("GE");
    
    cout << a.getFirst() << endl;
    cout << a.getLast() << endl;
    cout << a.size() << endl;
    
    a.removeFirst();
    a.removeLast();
    
    cout << a.size() << endl;
    
    return 0;
}
