//13-NOV-2016
//Tihomir Mladenov, F-61480
//Dequeue + LinkedList

#include <iostream>
#include <string>

using namespace std;

template <typename T>
class Node {
public:
    T data;
    Node<T>* next;
    Node<T>* prev;
};

template <typename T>
class Queue {
private:
    Node<T>* first;
    Node<T>* last;
    int sizes = 1;
public:
    Queue() {
        first = NULL;
        last = NULL;
    }
    
    bool isEmpty() {
        return (first == NULL);
    }
    
    virtual void insertFirst(T stuff) {
        
        Node<T>* tmp = new Node<T>;
        
        tmp->data = stuff;
        tmp->next = NULL;
        tmp->prev = NULL;
        
        if(isEmpty()) {
            first = last = tmp;
        }
        else {
            tmp->next = first;
            first->prev = tmp;
            first = tmp;
            sizes++;
        }
    }
    
    virtual T removeFirst() {
        if(isEmpty()) {
            throw string("Empty.\n");
        }
        T tmp = first->data;
        
        Node<T>* tmpnode = first;
        
        if(first->next != NULL) {
            first = first->next;
            first->prev = NULL;
        }
        
        else {
            first = NULL;
        }
        
        sizes--;
        delete tmpnode;
        
        return tmp;
    }
    
    virtual void insertLast(T Stuff) {
        Node<T>* tmp = new Node<T>;
        tmp->data = Stuff;
        tmp->next = NULL;
        tmp->prev = NULL;
        
        if(isEmpty()) {
            first = last = tmp;
        }
        else {
            last->next = tmp;
            tmp->prev = last;
            last = tmp;
        }
        sizes++; //check if here is ok
    }
    
    virtual T removeLast() {
        if(isEmpty()) {
            throw string("Empty.\n");
        }
        
        T tmp = last->data;
        
        Node<T>* tmpnode = last;
        
        if(last->prev != NULL) {
            last = last->prev;
            last->next = NULL;
        }
        else {
            last = NULL;
        }
        sizes--;
        delete tmpnode;
        
        return tmp;
        
    }
    
    virtual void front() {
        Node<T> *p = new Node<T>;
        
        p = first;
        
        if(isEmpty()) {
            cout << "Empty.\n" << endl;
        }
        else
            cout << p->data << endl;
    }
    
    virtual void back() {
        Node<T> *a = new Node<T>;
        
        a = last;
        if(isEmpty()) {
            cout << "Empty.\n";
        }
        else
            cout << a->data << endl;
    }
    
    virtual void size() {
        cout << sizes << endl;
    }
    
    virtual void clear() {
        while(first != NULL) {
            removeFirst();
        }
    }
    
    ~Queue() {
        clear();
    }
};


int main() {
    
    Queue<string> a;
    
    a.insertFirst("AB");
    a.insertLast("EA");
    
    a.size();
    
    a.removeLast();
    a.removeFirst();
    
    a.front();
    a.back();
    a.size();
    
    a.insertFirst("FAF");
    a.insertLast("BAB");
    
    a.front();
    a.back();
    
    a.size();
    
    return 0;
}
