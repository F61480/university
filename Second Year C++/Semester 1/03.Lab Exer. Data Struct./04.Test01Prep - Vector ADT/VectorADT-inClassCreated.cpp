#include <iostream>
#include <string>
#include <algorithm>

using namespace std;

class Cvector {
public:
	Cvector();
	Cvector(int initc, int maxc);
	int max_size;
	int init_size;
	int size;
	void insertAtRank(int r, string s);
	string atrank(int p);
	string* arr;
	~Cvector();
	void push_back(string s);
	void resize();
	void print();
	void sort();
};

Cvector::Cvector() {
	max_size = 8;
	init_size = 32;
	size = 0;
	arr = new string[init_size];
}

Cvector::Cvector(int initc, int maxc) {
	init_size = initc;
	max_size = max(initc, maxc);
	size = 0;
	arr = new string[init_size];
}

Cvector::~Cvector() {
	delete[] arr;

	size = 0;
	init_size = 0;
	max_size = 0;
}

void Cvector::resize() {
	if (size < init_size) {
		return;
	}
	init_size = min(init_size * 2, max_size);
	string *narr = new string[init_size];

	for (int i = 0; i < size; i++) {
		narr[i] = arr[i];
	}

	delete[] arr;
	arr = narr;

}

void Cvector::push_back(string s) {
	if (size < max_size) {
		resize();
		arr[size] = s;
		size++;
	}
	else {
		throw "Overflow.\n";
	}
}


void Cvector::print() {
	cout << "Inital size: " << init_size << endl;
	cout << "Max size: " << max_size << endl;

	for (int i = 0; i < size; i++) {

		cout << arr[i] << " ";
	}

	cout << endl;
}

/*Insert at Rank new element.
 *Int r = the index of the new element;
 *string s = the data;
 *output:
 *features:
*/
void Cvector::insertAtRank(int r, string s) {
	if (r < size) {
		arr[r] = s;
		size++;
	}
	else {
		throw "Overflow.\n";
	}
}

string Cvector::atrank(int r) {
	if (r < size) {
		return arr[r];
	}
	else
		throw "Overflow.\n";
}

void Cvector::sort() {
	string tmp;
	for (int i = 0; i < size; i++) {
		for (int j = 0; i < size - 1; i++) {
			if (arr[j] < arr[j + 1]) {
				tmp = arr[j];
				arr[j] = arr[j + 1];
				arr[j + 1] = tmp;
			}
		}
	}
}


int main() {
	Cvector v(3, 10);
	v.print();

	v.push_back("alala");
	v.push_back("bababa");

	v.print();

	v.push_back("ahahahah");
	v.push_back("afhafhhaf");

	v.print();

	/*v.atrank(0, "null");
	cout << v.atrank(0);
	v.print();*/


	system("pause");
	return 0;
}