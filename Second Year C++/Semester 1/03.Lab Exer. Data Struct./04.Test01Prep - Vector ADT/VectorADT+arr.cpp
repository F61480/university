//14-NOV-2016
//Tihomir Mladenov, F-61480
//Vector ADT w/ Array implementation

#include <iostream>
#include <string>

using namespace std;

template <typename T>
class Vector {
private:
    int elemCount;
    int sizes;
    T* arr;
public:
    Vector(int s = 16) {
        elemCount = s;
        sizes = 0;
        arr = new T[elemCount];
    }
    
    int size() {
        return sizes;
    }
    
    bool isEmpty() {
        return (sizes == 0);
    }
    
    virtual T elementAtRank(int r) {
        if(isEmpty()) {
            throw string("Vector's empty.\n");
        }
        
        return arr[r];
    }
    
    virtual void replaceAtRank(int r, T stuff) {
        arr[r] = stuff;
    }
    
    virtual void removeAtRank(int r) {
        if(isEmpty()) {
            throw string("Vector is Empty.\n");
        }
        int i = r;
        while(i < sizes - 1) {
            arr[i] = arr[i + 1];
            i++;
        }
        sizes--;
    }
    
    virtual void insertAtRank(int r, T Stuff) {
        if(sizes == elemCount) {
            vResize();
        }
        
        for(int i = r; i <= sizes - 1; i++) {
            arr[i + 1] = arr[i];
        }
        arr[r] = Stuff;
        sizes++;
    }
    
    virtual void vResize() {
        elemCount = 2 * elemCount;
        T* newVec = new T[elemCount];
        
        int i = 0;
        
        while(i < sizes) {
            newVec[i] = arr[i];
            i++;
        }
        
        delete [] arr;
        arr = newVec;
    }
    
    
    ~Vector() {
        delete [] arr;
    }
    
};


int main() {
    
    Vector<int> a(3);
    
    a.insertAtRank(0, 1);
    a.insertAtRank(1, 3);
    a.insertAtRank(2, 4);
    
    cout << a.size() << endl;
    cout << a.elementAtRank(0) << endl;
    cout << a.elementAtRank(1) << endl;
    cout << a.elementAtRank(2) << endl;
    
    
    
    return 0;
}
