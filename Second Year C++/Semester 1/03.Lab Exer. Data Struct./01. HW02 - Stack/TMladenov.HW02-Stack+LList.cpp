//30-OCT-2016
//Tihomir Mladenov, F-61480
//Stack + LinkedList

#include <iostream>
#include <string>

using namespace std;

template <typename T>
class Node {
public:
    T data;
    Node * next;
};

template <typename T>
class Stack {
private:
    int sizes = 1;
public:
    Node<T> *topS;
    Stack() {
        topS = NULL;
    }

    /*bool isEmpty() {
        return (topS == NULL);
    }*/

    void push(T entry) {
        Node<T>* tmp;

        tmp = new Node<T>;
        (*tmp).data = entry;
        (*tmp).next = NULL;

        if(topS != NULL) {
            (*tmp).next = topS;
            sizes++;
        }
        topS = tmp;
    }

    void pop() {
        Node<T>* tmp2;

        if(topS == NULL) {
            cout << "Stack's empty.\n";
        }
        tmp2 = topS;
        topS = (*topS).next;
        sizes--;
        delete tmp2;
    }

    void top() {
        cout << (*topS).data << endl;
    }
    
    void size() {
        cout << sizes << endl;
    }
};

int main() {

    Stack<string> a;

    for(int i = 0; i < 4; i++) {
        string tempEntry;
        cin >> tempEntry;

        a.push(tempEntry);
    }
    
    a.size();

    for(int i = 0; i < 4; i++) {
        a.top();
        a.pop();
    }
    
    a.size();

    return 0;
}
