//30-OCT-2016
//Tihomir Mladenov, F-61480
//Stack + Array implementation

#include <iostream>
#include <string>

using namespace std;

template<typename T>
class Stack {
private:
    int top1;
    int sizes;
    T *data;
public:
    Stack(T a1) {
        data = new T[a1];
        if(data == NULL) {
            throw string("Not enough memory.\n");
        }
        sizes = a1;
        top1 = -1;
    }
    bool isEmpty() {
        return (top1 < 0);
    }
    
    void top() {
        cout << data[top1] << endl;
    }
    
    void push(T tmp) {
        if(top1 == sizes - 1) {
            throw string("Stack overflow.\n");
        }
        else {
            top1++;
            data[top1] = tmp;
        }
    }
    T pop() {
        /*if(isEmpty) {
            throw string("Stack's empty.\n");
        }*/
        if(data == 0 || top1 == -1) {
            throw string("Stack's empty.\n");
        }
        T tmp = data[top1];
        top1--;
        return tmp;
    }
    
    void size() {
        cout << sizes << endl;
    }
    
    ~Stack() {
        delete [] data;
        
        data = NULL;
        sizes = 0;
        top1 = -1;
    }
    
    
};

int main() {
    
    Stack<int> test(4);
    
    for(int i = 0; i < 4; i++) {
        int entry;
        cin >> entry;
        test.push(entry);
    }
    
    test.size();
    
    for(int i = 0; i < 4; i++) {
        test.top();
        test.pop();
    }
    
    return 0;
}
