//Tihomir Mladenov, F-61480
//Queue + DLinkedList

#include <iostream>
#include <string>

using namespace std;

template <typename T>
class Node {
public:
    T data;
    Node<T>* next;
    Node<T>* prev;
};

template <typename T>
class Queue {
private:
    Node<T>* first;
    Node<T>* last;
    int sizes = 1;
public:
    Queue() {
        first = NULL;
        last = NULL;
    }
    
    bool isEmpty() {
        return (first == NULL);
    }
    
    virtual void enqueue(T stuff) {
        Node<T>* tmp = new Node<T>;
        
        tmp->data = stuff;
        tmp->next = NULL;
        tmp->prev = NULL;
        
        if(isEmpty()) {
            first = last = tmp;
            first->prev = NULL;
            sizes++;
        }
        else {
            last->next = tmp;
            tmp->prev = last;
        }
        last = tmp;
    }
    
    virtual T dequeue() {
        Node<T>* tmp = new Node<T>;
        if(isEmpty()) {
            throw string("Queue is empty.\n");
        }
        else {
            T data_del = first->data;
            tmp = first;
            first = first->next;
            last = last->prev;
            sizes--;
            delete tmp;
            return data_del;
        }
    }
    
    virtual void front() {
        Node<T> *p = new Node<T>;
        
        p = first;
        
        if(isEmpty()) {
            cout << "Empty.\n" << endl;
        }
        else
            cout << p->data << endl;
    }
    
    virtual void back() {
        Node<T> *a = new Node<T>;
        
        a = last;
        if(isEmpty()) {
            cout << "Empty.\n";
        }
        else
            cout << a->data << endl;
    }
    
    virtual void size() {
        cout << sizes << endl;
    }
    
    virtual void clear() {
        while(first != NULL) {
            dequeue();
        }
    }
    
    ~Queue() {
        clear();
    }
};


int main() {
    
    Queue<string> a;
    
    a.enqueue("a");
    a.enqueue("b");
    a.front();
    a.size();
    a.dequeue();
    a.front();
    a.size();
    a.dequeue();
    a.size();
    
    return 0;
}
