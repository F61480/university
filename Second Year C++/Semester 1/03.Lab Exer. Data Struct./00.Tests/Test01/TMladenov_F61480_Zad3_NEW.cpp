//Tihomir Mladenov, F-61480
//Queue + Array + max_cap

#include <iostream>
#include <string>
#include <cstdlib>

using namespace std;

template<typename Temp>
class CQueue {
private:
    int qfirst;
    int qlast;
    int elemCount;
    int max_el;
    int max_cap;
    Temp* entry;
public:
    CQueue(int a, int b) {
        qfirst = 0;
        qlast = -1;
        max_el = a;
        max_cap = b;
        elemCount = 0;
        entry = new Temp[max_el];
    }
    
    bool isFull() {
        return (elemCount == max_el);
    }
    bool isEmpty() {
        return (elemCount == 0);
    }
    
    virtual void enqueue(Temp tmp) {
        if(isFull()) {
            resize();
        }
        qlast = (qlast + 1) % max_el;
        entry[qlast] = tmp;
        elemCount++;
    }
    virtual Temp dequeue() {
        Temp tmp2;
        if(isEmpty()) {
            throw "Queue is empty.";
        }
        
        tmp2 = entry[qfirst];
        qfirst = (qfirst + 1) % max_el;
        elemCount--;
        return tmp2;
    }
    
    virtual void front() {
        cout << entry[qfirst] << endl;
    }
    
    virtual void rear() {
        cout << entry[qlast] << endl;
    }
    
   virtual void size() {
        cout << "Size is: " << elemCount << endl;
    }
    
    virtual void resize() {
        
        if(elemCount >= max_el + max_cap) {
            throw "Queue Overflow.";
        }
        
        max_el += max_cap;
        max_cap = 0;
        Temp* tmp = new Temp[max_el];
        
        for(int i = 0; i < elemCount; i++) {
            tmp[i] = entry[i];
        }
        
        delete [] entry;
        entry = tmp;
        cout << "Resized. New size: " << max_el << endl;
     }
    
    ~CQueue() {
        delete [] entry;
        
        entry = NULL;
        elemCount = 0;
        qfirst = 0;
        qlast = -1;
    }
};


int main() {
    
    int first, second;
    cin >> first >> second;
    
    CQueue<string> a(first, second);
    
    for(int i = 0; i < first + second + 1; i++) {
        try {
            string entry;
            cin >> entry;
            
            a.enqueue(entry);
            a.size();
        }
        catch (const char* err) {
            cerr << err << endl;
            exit(1);
        }
    }
    
    /*CQueue<string> a(3, 7);
    
    for(int i = 0; i < 8; i++) {
        try {
            string entry;
            cin >> entry;
            
            a.enqueue(entry);
            a.size();
        }
        catch (const char* err) {
            cerr << err << endl;
            exit(1);
        }
    }*/
    
    return 0;
}
