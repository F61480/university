//Tihomir Mladenov, F-61480
//Stack + Array implementation + max_cap

#include <iostream>
#include <string>
#include <cstdlib>

using namespace std;

template<typename T>
class Stack {
private:
    int top1;
    int sizes;
    int element_count;
    int max_cap;
    T *data;
public:
    Stack(int a, int b) {
        data = new T[a];
        if(data == NULL) {
            throw "Not enough memory.";
        }
        element_count = a;
        max_cap = b;
        sizes = 0;
        top1 = -1;
    }
    
    bool max_cap_check() {
        return (sizes == element_count);
    }
    
    bool isEmpty() {
        return (top1 < 0);
    }
    
    void top() {
        cout << "Top: " << data[top1] << endl;
    }
    
    void push(T tmp) {
        if(max_cap_check()) {
            stResize();
        }
        
        if(sizes >= element_count + max_cap) {
            throw "Queue Overflow.";
        }
        
        top1++;
        sizes++;
        data[top1] = tmp;
    }
    T pop() {
        if(data == 0 || top1 == -1) {
            throw "Stack's empty.";
        }
        T tmp = data[top1];
        sizes--;
        top1--;
        return tmp;
    }
    
    void size() {
        cout << "Size is: " << sizes << endl;
    }
    
    virtual void stResize() {
        
        element_count += max_cap;
        max_cap = 0;
        T* newStack = new T[element_count];
            
        for(int i = 0; i < sizes; i++) {
            newStack[i] = data[i];
        }
        delete [] data;
        data = newStack;
        cout << "Resized. New size: " << element_count << endl;
    }
    
    ~Stack() {
        delete [] data;
        
        data = NULL;
        sizes = 0;
        top1 = -1;
    }
    
    
};

int main() {
    
    int min_cap, max_resize;
    cin >> min_cap >> max_resize;
    
    if(max_resize < min_cap) {
        cout << "max_resize must be > min_cap.\n";
        return 1;
    }
    
    Stack<string> test(min_cap, max_resize);
    
    for(int i = 0; i < min_cap + max_resize + 1; i++) {
        try {
            string entry;
            cin >> entry;
            test.push(entry);
            test.top();
            test.size();
        }
        catch (const char* err) {
            cerr << err << endl;
            exit(1);
        }
    }
    
    /*Stack<string> test(3, 7);
    
    for(int i = 0; i < 9; i++) {
        try {
            string entry;
            cin >> entry;
            test.push(entry);
            test.top();
            test.size();
        }
        catch (const char* err) {
            cerr << err << endl;
            exit(1);
        }
    }*/
    
    return 0;
}
