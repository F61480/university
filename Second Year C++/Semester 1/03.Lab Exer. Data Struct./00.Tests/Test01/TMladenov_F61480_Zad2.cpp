//Tihomir Mladenov, F-61480
//Stack + Array implementation + max_cap

#include <iostream>
#include <string>

using namespace std;

template<typename T>
class Stack {
private:
    int top1;
    int sizes;
    int element_count;
    int max_cap;
    T *data;
public:
    Stack(int a, int b) {
        data = new T[a];
        if(data == NULL) {
            throw string("Not enough memory.\n");
        }
        element_count = a;
        max_cap = b;
        sizes = 0;
        top1 = -1;
    }
    bool isEmpty() {
        return (top1 < 0);
    }
    
    void top() {
        cout << data[top1] << endl;
    }
    
    void push(T tmp) {
        if(sizes == element_count) {
            stResize();
        }
        top1++;
        sizes++;
        data[top1] = tmp;
    }
    T pop() {
        if(data == 0 || top1 == -1) {
            throw string("Stack's empty.\n");
        }
        T tmp = data[top1];
        sizes--;
        top1--;
        return tmp;
    }
    
    void size() {
        cout << sizes << endl;
    }
    
    virtual void stResize() {
        if(max_cap <= 0) {
            throw string("Max Cap reached. Overflow.\n");
        }
        
        element_count = 2 * element_count;
        T* newStack = new T[element_count];
            
        for(int i = 0; i < sizes; i++) {
            newStack[i] = data[i];
        }
        delete [] data;
        data = newStack;
        max_cap--;
    }
    
    ~Stack() {
        delete [] data;
        
        data = NULL;
        sizes = 0;
        top1 = -1;
    }
    
    
};

int main() {
    
    Stack<string> test(3, 1);
    
    for(int i = 0; i < 7; i++) {
        string entry;
        cin >> entry;
        
        test.push(entry);
        test.top();
        test.size();
    }
    
    return 0;
}
