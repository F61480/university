//Tihomir Mladenov, F-61480
//Queue + Array + max_cap

#include <iostream>
#include <string>

using namespace std;

template<typename Temp>
class CQueue {
private:
    int qfirst;
    int qlast;
    int elemCount;
    int max_el;
    int max_cap;
    Temp* entry;
public:
    CQueue(int a, int b) {
        qfirst = 0;
        qlast = -1;
        max_el = a;
        max_cap = b;
        elemCount = 0;
        entry = new Temp[max_el];
    }
    bool isFull() {
        return (elemCount == max_el - 1);
    }
    bool isEmpty() {
        return (elemCount == 0);
    }
    
    virtual void enqueue(Temp tmp) {
        if(isFull()) {
            resize();
        }
        else {
            qlast = (qlast + 1) % max_el;
            entry[qlast] = tmp;
            elemCount++;
        }
    }
    virtual Temp dequeue() {
        Temp tmp2;
        if(isEmpty()) {
            throw string("Queue is empty.\n");
        }
        else {
            tmp2 = entry[qfirst];
            qfirst = (qfirst + 1) % max_el;
            elemCount--;
        }
        return tmp2;
    }
    
    virtual void front() {
        cout << entry[qfirst] << endl;
    }
    
    virtual void rear() {
        cout << entry[qlast] << endl;
    }
    
   virtual void size() {
        cout << "Size is: " << elemCount + 1 << endl;
    }
    
    virtual void resize() {
        if(max_cap <= 0) {
            throw string("Max Cap reached. Overflow.\n");
        }
        
        max_el *= 2;
        Temp* tmp = new Temp[max_el];
        
        for(int i = 0; i < elemCount; i++) {
            tmp[i] = entry[i];
        }
        
        delete [] entry;
        entry = tmp;
        max_cap--;
     }
    
    ~CQueue() {
        delete [] entry;
    }
};


int main() {
    
    CQueue<string> a(3, 2);
    
    for(int i = 0; i < 7; i++) {
        string entry;
        cin >> entry;
        
        a.enqueue(entry);
    }
    
    a.front();
    a.rear();
    a.size();
    
    return 0;
}
