#include <iostream>
#include <string>

using namespace std;

class RuntimeException {
    public:
        RuntimeException(const string& err) { errorMsg = err; } // Конструктур за Ексепшъни. Приема стринг с име на грешката от всеки обект;
        string getMessage() const { return errorMsg; } //функция за връщане на текста на грешката;

    private:
        string errorMsg; // променлива, в която се запазва подадената грешка;

};

class QueueEmptyException : public RuntimeException {
    public:
        QueueEmptyException() : RuntimeException("Empty Queue!") {} //Първи роизводен клас за връщане на конкретно изключение за ПРАЗНА Опашка;
};

class QueueOverflowException : public RuntimeException {
    public:
        QueueOverflowException() : RuntimeException("Queue overflow!") {} //Втори производен клас за връщане на конкретно изключение за ПЪЛНА Опашка;
};



template <typename TElement> // Темплейт за всякакъв тип данни, в ТЕлемент се чака типът данна при изпълнение;
class CQueue {
public:
    CQueue() {};
    virtual ~CQueue() {};

    virtual void enqueue(TElement* pInEl) throw(QueueOverflowException) = 0; // приема указател към подадения тип данна за вкарване. Може да хвърля изключение за Оувърфлоу;
    virtual TElement* dequeue() throw(QueueEmptyException) = 0; // При изпълнение на функцията се премахва един елемент. Може да изхвърля изключение за празна Опашка;
};

template <typename TElement> 
class MQueue: public CQueue<TElement> {
private:
    int m_qTop; //Горна граница да опашката;
    int m_qBottom; // Долна граница на опашката;
    int m_nCap; // размер на опашката;
    TElement** m_pElements; // Масив за самата опашка, в която ще се вкарват променливите;
public:
    MQueue() {
        m_qTop = -1;
        m_qBottom = -1;
        m_nCap = 256;
        m_pElements = new TElement*[m_nCap];
    }
    virtual ~MQueue() {
        delete[] m_pElements;
    }

    virtual void enqueue(TElement* pInEl) throw(QueueOverflowException) {
        if (m_qTop < m_nCap - 1) {
            m_qTop++;
            m_pElements[m_qTop] = pInEl;
        }
        else {
            throw QueueOverflowException();
        }
    }
    virtual TElement* dequeue() throw(QueueEmptyException) {
        if(m_qBottom < 0) {
            throw QueueEmptyException();
        }
        
        if(m_qBottom < m_nCap - 1) {
            m_qBottom++;
            return m_pElements[m_qBottom];
        }
    }
    
    void getLast() const {
        cout << *m_pElements[m_qTop] << endl;
    }
};

int main() {
    MQueue<int> a;
    
    for(int i = 0; i < 5; i++) {
        int p;
        cin >> p;
        int* temp = &p;
        
        a.enqueue(temp);
    }
    
    return 0;
}
