//06-NOV-2016
//Tihomir Mladenov, F-61480
//Circular Queue + Array

#include <iostream>
#include <string>

using namespace std;

#define SIZE 256

template<typename Temp>
class CQueue {
private:
    int qfirst;
    int qlast;
    int elemCount;
    Temp* entry;
public:
    CQueue() {
        qfirst = 0;
        qlast = -1;
        elemCount = 0;
        entry = new Temp[SIZE];
    }
    bool isFull() {
        return (elemCount == SIZE);
    }
    bool isEmpty() {
        return (elemCount == 0);
    }
    
    virtual void enqueue(Temp tmp) {
        if(isFull()) {
            throw string("Queue overflow.\n");
        }
        else {
            qlast = (qlast + 1) % SIZE;
            entry[qlast] = tmp;
            elemCount++;
        }
    }
    virtual Temp dequeue() {
        Temp tmp2;
        if(isEmpty()) {
            throw string("Queue is empty.\n");
        }
        else {
            tmp2 = entry[qfirst];
            qfirst = (qfirst + 1) % SIZE;
            elemCount--;
        }
        return tmp2;
    }
    
    //NOT ATD!!!
    virtual void print() {
        for(int i = qfirst; i <= qlast; i++) {
            cout << "Element: " << entry[i] << endl;
        }
    }
    
    virtual void front() {
        cout << entry[qfirst] << endl;
    }
    
   virtual void size() {
        cout << "Size is: " << elemCount << endl;
    }
    
    ~CQueue() {
        delete [] entry;
    }
};


int main() {
    
    CQueue<string> a;
    
    //Replace with 4 or 5 to test.
    for(int i = 0; i < 256; i++) {
        string b;
        cin >> b;
        
        a.enqueue(b);
    }
    
    a.front();
    
    a.print();
    a.size();
    
    for(int i = 0; i < 256; i++) {
        a.dequeue();
    }
    
    a.print();
    a.size();
    
    return 0;
}
