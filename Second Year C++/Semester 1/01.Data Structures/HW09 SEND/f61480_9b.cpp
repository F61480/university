#include <iostream>
#include <cstdio>

using namespace std;

unsigned size = 0;
const int maxs = 150;
const int maxValue = 10000;
int tmpSum = 0;
int sumMin = 0;

unsigned cycleMin[maxs];
unsigned cycle[maxs];
int matrix[maxs][maxs];
char inUse[maxs];

void print() {
    unsigned check = 0;
	
    for(unsigned i = 0; i < size-1; i++) {
		if (1 == cycleMin[i] + 1) {
            check++;
        }
	}
	if(check == size-1) {
		cout << "-1";
	}
	else {
        cout << "1 ";
		for(unsigned i = 0; i < size-1; i++) {
			cout << cycleMin[i] + 1 << " ";
		}
        cout << endl;
	}
}

void recCalcul(unsigned i, unsigned l) {
	if ((0 == i) && (0 < l)) {
		if (l == size) {
			sumMin = tmpSum;
			for(unsigned k = 0; k < size; k++) {
                cycleMin[k] = cycle[k];
            }
		}
		return;
	}
	if (inUse[i]) {
        return;
    }
    inUse[i] = 1;
    
    for(unsigned k = 0; k < size; k++)
		
        if (k != i && matrix[i][k]) {
			cycle[l] = k;
			tmpSum += matrix[i][k];
			
            if (sumMin > tmpSum)  {
                recCalcul(k, l + 1);
            }
            
            tmpSum -= matrix[i][k];
		}
        inUse[i] = 0;
}

void initializeMatrix() {
    for (unsigned i = 0; i < size; i++) {
			for (unsigned j = 0; j < size; j++) {
				matrix[i][j] = 0;
			}
		}
}

void MaxFind(unsigned entry, int findMax, int* myarr) {
    for (unsigned i = 0; i < entry * 3; i++) {
            cin >> myarr[i];
			myarr[i] -= 1;
			
            if (0 == i) {
                findMax = myarr[i];
            }
			else if (findMax < myarr[i]) {
                findMax = myarr[i];
            }
		}
		findMax += 1;
		size = findMax;
}

int main() {
	
    unsigned entry;
	
    while (cin >> entry) {
        int	findMax = 0;
        int* myarr = new int[entry * 3];
        initializeMatrix();
		MaxFind(entry, findMax, myarr);
		
        for (unsigned i = 0; i < ((entry * 3) - 1); i += 1) {
			matrix[myarr[i]][myarr[i + 1]] = 1;
		}
		for (unsigned j = 0; j < size; j++) {
            inUse[j] = 0;
        }
        
		sumMin = maxValue;
		tmpSum = 0;
		cycle[0] = 1;
		recCalcul(0, 0);
		print();
		for (unsigned i = 0; i < size - 1; i++) {
			cycleMin[i] = 0;
		}
	}
	return 0;
}
