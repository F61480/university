//11 - NOV - 2016
//Tihomir Mladenov, F-61480
//Homework 02 - Exer. 2a

#include <iostream>

using namespace std;

#define SIZE 10000

int a = 0;
long long b = 0;
long long digit_count = 0;
long long arr[SIZE];
long long arr2[SIZE];
long long fib[SIZE];

int main() {
    
    while(cin >> a >> b >> digit_count) {
        for(int i = 0; i < digit_count; i++) {
            cin >> arr[i];
        }
        
        fib[0] = fib[1] = 1;
        
        for(int i = 2; i < SIZE; i++) {
            fib[i] = (a * fib[i - 1]) + (b * fib[i - 2]);
        }
        
        for(int i = 0; i < digit_count; i++) {
            arr2[i] = 0;
            
            for(int j = 1; j < SIZE; j++) {
                if(arr[i] == fib[j]) {
                    arr2[i] = ++j;
                }
            }
        }
        for(int i = 0; i < digit_count; i++) {
            cout << arr2[i] << endl;
        }
    }
    
    return 0;
}
