//07 - NOV - 2016
//Tihomir Mladenov, F-61480
//Homework 02 - Exer. 2b

#include<iostream>
#include<ios>

using namespace std;

int reduce(int a, int b) {
    while(a != b) {
        if(a > b) {
            a = a - b;
        }
        else {
            b = b - a;
        }
    }
    return a;
}
 
int main ()
{
    int ch1, zn1, ch2, zn2;
    char sign;
    
    while(cin >> ch1 >> sign >> zn1 >> sign >> ch2 >> sign >> zn2) {
        
        int top = ch1 * zn2 + ch2 * zn1;
        int bottom = zn1 * zn2;
        
        if(top == 0) {
            cout << 0 << endl;
            return 0;
        }
        if (top % bottom == 0){
            cout << top / bottom << endl;
        }
        else {
            cout << top / reduce(top, bottom) << "/" << bottom / reduce(top, bottom) << endl;
        }
    }
    return 0;
}
