#include <iostream>
#include <vector>
#include <string>
#include <sstream>
#include <cmath>
#include <climits>

using namespace std;

#define init_size 17000

int main() {

    string entry;

    while(getline(cin, entry)) {
        
        istringstream instr(entry);
        
        vector<int> myvec;
        vector<int> sumVec(init_size, 0);
        vector<int> sumVec2(init_size, 0);
        
        int digit = 0;

        while(instr >> digit) {
            myvec.push_back(digit);
        }

        int ind = 0;

        for(int i = 0; i < init_size - 1; i++) {
            for(int j = 0; j < myvec.size(); j++) {
                if((i >> j) & 1) {
                    sumVec[ind] += myvec[j];

                } 
                else {
                    sumVec2[ind] += myvec[j];
                }
            }
            ind++;
        }
        
        int max = INT_MAX;
        int ind2 = 0;
        
        for(int i = 0; i < ind; i++) {
            if(abs(sumVec[i] - sumVec2[i]) < max) {
                ind2 = i;
                max = abs(sumVec[i] - sumVec2[i]);
            }
        }
        
        if(sumVec[ind2] < sumVec2[ind2]) {
            cout << sumVec[ind2] << " " << sumVec2[ind2];
        } 
        else {
            cout << sumVec2[ind2] << " " << sumVec[ind2];
        }

        cout << endl;
        myvec.clear();
        sumVec.clear();
        sumVec2.clear();
    }

    return 0;
}
