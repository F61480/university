#include <iostream>
#include <vector>

using namespace std;

int search(vector<int>& myvec, int l, int r, int t)
{
    while(r - l > 1) {
        int m = l + (r - l) / 2;

        if(myvec[m] >= t)
            r = m;
        else
            l = m;
    }

    return r;
}

int main()
{

    int data;

    while(cin >> data) {
        
        vector<int> myvec;
        int size = 0;

        for(int i = 0; i < data; i++) {
            int tmp;
            cin >> tmp;
            myvec.push_back(tmp);
        }

        if(myvec.size() == 0) {
            return 0;
        }

        vector<int> myvecSec(myvec.size(), 0);
        
        int pos = 1;

        myvecSec[0] = myvec[0];
        
        size = myvec.size();
        
        for(int i = 1; i < size; i++) {
            if(myvec[i] < myvecSec[0]) {
                myvecSec[0] = myvec[i];
            } else if(myvec[i] >= myvecSec[pos - 1]) {
                myvecSec[pos++] = myvec[i];
            }
            else {
                myvecSec[search(myvecSec, -1, pos - 1, myvec[i])] = myvec[i];
            }
        }

        cout << pos << endl;
        myvec.clear();
        myvecSec.clear();
    }

    return 0;
}
