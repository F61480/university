#include <algorithm>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <cstring>
#include <stack>

using namespace std;

int main()
{
    int n;
    
    while(cin >> n) {
        
        vector<string> myvec;
        stack<string> mystack;
        
        for(int i = 0; i < n; i++) {
            string tmp;
            cin >> tmp;
            myvec.push_back(tmp);
        }
        for(int i = 0; i < myvec.size(); i++) {
            if(mystack.empty()) {
                mystack.push(myvec[i]);
            }
            else {
                if(mystack.top() == myvec[i]) {
                    mystack.push(myvec[i]);
                }
                else {
                    mystack.pop();
                }
            }
        }
        if(mystack.empty()) {
            cout << endl;
        }
        else {
            cout << mystack.top() << endl;
        }
        
    }

    return 0;
}
