#include <iostream>
#include <list>
#include <queue>

using namespace std;

#define arcSizeX 1000
#define arcSizeY 2

class graph
{
private:
    int size;
    list<int>* mylist;
public:
    graph(int size)
    {
        this->size = size;
        mylist = new list<int>[size];
    }

    void addArc(int a, int b)
    {
        mylist[a].push_back(b);
    }

    void BFS(int tmp)
    {
        list<int> myqueue;
        bool* isVisited = new bool[size];

        for(int i = 0; i < size; i++) {
            isVisited[i] = false;
        }

        isVisited[tmp] = true;
        myqueue.push_back(tmp);

        list<int>::iterator it;

        do {
            tmp = myqueue.front();
            
            cout << tmp << " ";
            
            myqueue.pop_front();

            for(it = mylist[tmp].begin(); it != mylist[tmp].end(); ++it) {
                if(!isVisited[*it]) {
                    isVisited[*it] = true;
                    myqueue.push_back(*it);
                }
            }
        } while(!myqueue.empty());
    }
};

int main()
{
    int arcS[arcSizeX][arcSizeY] = {0};

    int arc;
    cin >> arc;

    int max = 0;
    int i = 0;
    int j = 0;

    do {
        int tmp_1, tmp_2;
        cin >> tmp_1 >> tmp_2;

        if((tmp_1 > tmp_2) && (tmp_1 > max)) {
            max = tmp_1;
        }

        if((tmp_1 < tmp_2) && (tmp_2 > max)) {
            max = tmp_2;
        }

        arcS[i][0] = tmp_1;
        arcS[i][1] = tmp_2;
        i++;

    } while(i < arc);

    graph depth(max + 1);

    do {
        depth.addArc(arcS[j][0], arcS[j][1]);
        j++;
    } while(j < arc);

    depth.BFS(1);

    return 0;
}
