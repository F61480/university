#include <iostream>
#include <list>
#include <queue>

using namespace std;

#define arcSizeX 1000
#define arcSizeY 2

class graph
{
private:
    int size;
    list<int>* mylist;
    
    void locate(int a, bool isVisited[])
    {
        isVisited[a] = true;
        cout << a << " ";

        list<int>::iterator it;

        for(it = mylist[a].begin(); it != mylist[a].end(); it++) {
            if(!isVisited[*it]) {
                locate(*it, isVisited);
            }
        }
    }

public:
    graph(int size)
    {
        this->size = size;
        mylist = new list<int>[size];
    }

    void addArc(int a, int b)
    {
        mylist[a].push_back(b);
    }

    void DFS(int tmp)
    {
        int i = 0;
        bool* isVisited = new bool[tmp];

        do {
            isVisited[i] = false;
            i++;
        } while(i < tmp);

        locate(tmp, isVisited);
    }
};

int main()
{
    int arcS[arcSizeX][arcSizeY] = { 0 };

    int arc;
    cin >> arc;

    int max = 0;
    int i = 0;
    int j = 0;

    do {
        int tmp_1, tmp_2;
        cin >> tmp_1 >> tmp_2;

        if((tmp_1 > tmp_2) && (tmp_1 > max)) {
            max = tmp_1;
        }

        if((tmp_1 < tmp_2) && (tmp_2 > max)) {
            max = tmp_2;
        }

        arcS[i][0] = tmp_1;
        arcS[i][1] = tmp_2;
        i++;

    } while(i < arc);

    graph depth(max + 1);

    do {
        depth.addArc(arcS[j][0], arcS[j][1]);
        j++;
    } while(j < arc);

    depth.DFS(1);

    return 0;
}
