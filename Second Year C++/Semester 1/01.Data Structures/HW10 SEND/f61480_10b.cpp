#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <sstream>

using namespace std;

int main()
{
    vector<string> myvec;

    string entry;
    while(getline(cin, entry)) {

        istringstream sInp(entry);
        string tmp;

        while(sInp >> tmp) {
            sort(tmp.begin(), tmp.end());
            myvec.push_back(tmp);
        }
        
        sort(myvec.begin(), myvec.end());
        
        int k = 1;
        int wSize = myvec.size() - 1;
        
        for(int i = 0; i < wSize; i++) {
            if(myvec[i] != myvec[i + 1]) {
                k++;
            }
        }
        cout << k << endl;
        myvec.clear();
    }

    return 0;
}
