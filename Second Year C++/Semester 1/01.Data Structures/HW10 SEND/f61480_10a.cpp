#include <iostream>
#include <cstdio>
#include <cstring>

using namespace std;

const int bSize = 100000051;
bool mybool[bSize];
int size = 100000050;

void ArrInitialize()
{
    for(int i = 2; i * i <= size; i++) {
        if(true == mybool[i]) {
            for(int j = i * 2; j <= size; j += i) {
                mybool[j] = false;
            }
        }
    }

    mybool[0] = false;
    mybool[1] = false;
}

void checkA(char& entry)
{
    if(entry == 'A' || entry == 'a') {
        int a, b;
        cin >> a >> b;
        if(b < a) {
            swap(a, b);
        }
        int counter = 0;
        for(int i = a; i <= b; i++) {
            if(mybool[i] == true) {
                counter++;
            }
        }
        cout << counter << endl;
    }
}

void checkB(char& entry)
{
    if(entry == 'B' || entry == 'b') {
        int a;
        cin >> a;
        if(mybool[a] == true) {
            cout << a << endl;

        } else if(0 == a) {
            cout << 0 << endl;

        } else if(1 == a) {
            cout << 1 << endl;
        } else
            for(int i = 0; i < size; i++) {
                if(mybool[i] == true) {
                    if(0 == a % i) {
                        cout << i << endl;
                        break;
                    }
                }
            }
    }
}

void checkC(char& entry)
{
    if(entry == 'C' || entry == 'c') {
        int a;
        cin >> a;
        if(mybool[a] == true) {
            cout << 1 << endl;
        } else
            cout << 0 << endl;
    }
}

void checkD(char& entry)
{
    if(entry == 'D' || entry == 'd') {
        int a, b, c = 0;
        cin >> a;

        while(a != 0) {

            b = a % 10;
            c = c * 10 + b;
            a = a / 10;
        }
        if(mybool[c] == true) {
            cout << 1 << endl;
        } else
            cout << 0 << endl;
    }
}

void checkE(char& entry)
{
    if(entry == 'E' || entry == 'e') {
        int a;
        cin >> a;
        if(mybool[a]) {
            cout << a << endl;
        } else
            for(int i = 0; i < size; i++) {
                if(mybool[a + i] == true && mybool[a - i] == true) {
                    cout << a - i << " " << a + i << endl;
                    break;
                } else if(mybool[a + i] == true) {
                    cout << a + i << endl;
                    break;
                } else if(mybool[a - i] == true) {
                    cout << a - i << endl;
                    break;
                }
            }
    }
}

void checkF(char& entry)
{
    if(entry == 'F' || entry == 'f') {
        int a, counter = 0;
        cin >> a;
        for(int i = 2; i < a; i++) {
            if(mybool[i] == true)
                counter++;
        }
        cout << counter << endl;
    }
}

int main()
{
    memset(mybool, true, sizeof(mybool));
    ArrInitialize();

    char entry;
    while(cin >> entry) {

        checkA(entry);
        checkB(entry);
        checkC(entry);
        checkD(entry);
        checkE(entry);
        checkF(entry);

        if(entry == 'q') {
            return 0;
        }
    }
}
