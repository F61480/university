//#include <bits/stdc++.h>
#include <vector>
#include <iostream>
#include <algorithm>
#include <string>

using namespace std;


int main() {
    
    while(cin) {
        //Defining Number of Test Cases;
        int testCases;
        cin >> testCases;
        
        while(testCases > 0) {
            
            //Defining Array Size;
            unsigned long long arrSize;
            cin >> arrSize;
            
            vector<unsigned long long>arrOfDigits;
            
            //Pushing entered elements into the array;
            for(unsigned long long i = 0; i < arrSize; i++) {
                unsigned long long tmp;
                cin >> tmp;
                
                arrOfDigits.push_back(tmp);
            }
            
            //Sorting the array, first step of finding the Median;
            sort(arrOfDigits.begin(), arrOfDigits.end());
            
            //Copying the size of the array for shorter code;
            unsigned long long size = arrOfDigits.size();
            
            double median = 0;
            
            //Finding the Median;
            if((size % 2) == 0) {
                unsigned long long size_cpy = arrOfDigits.size() - 1;
                median = arrOfDigits[size_cpy / 2];
            }
            else {
                median = arrOfDigits[size / 2];
            }
            
            cout << median << endl;
            
            testCases--;
        }
    }
    
    
    return 0;
}
