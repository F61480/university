/*  Tihomir Mladenov - NBU CS 2nd year, 26 - NOV - 2016
 *  Exercise: You are given an integer of N digits. Find
 *  the largest N digit number that can be created  by
 *  switching the digit's location.
 *  Data Entry: Data entry until the end of the program
 *  The digits will be larger than 10^200              .*/


//#include <bits/stdc++.h>
#include <iostream>
#include <algorithm>
#include <string>

using namespace std;


int main() {
    
    while(cin) {
        string entry;
        cin >> entry;
        
        //Sorting the digit;
        sort(entry.begin(), entry.end());
        
        //Once we have it sorted, we reverse it to obtain the largest digit;
        reverse(entry.begin(), entry.end());
        cout << entry << endl;
    }
    
    
    return 0;
}
