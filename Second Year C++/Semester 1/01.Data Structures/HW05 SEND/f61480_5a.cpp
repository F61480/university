#include <iostream>
#include <iomanip>
#include <string>
#include <cfloat>
#include <cmath>

using namespace std;

long double func(long double x, long double a) {
    long double calc = x*x*x*x*x - a*x - (123456 % 100);
    return calc;
}

int main() {
    
    long double first, second, third;
	long double entry;
    bool check;
    
	while (cin >> entry) {
        
        long double fa, fb, fc = 0;
		check = true;
        
        fa = 0;
		fb = 10;
	 	   
		if ((func(fa, entry) * func(fb, entry)) > 0)
		{
			check = false;

		}
		else {
            for (int i = 0; i < 10000; i++) {
				fc = (fa + fb) / 2.0l;
                
				first = func(fa, entry);
				second = func(fb, entry);
				third = func(fc, entry);
				
                if (third == 0) {
					break;
				}

				if ((first * third) > 0) {
					fa = fc;
				}
				else if ((first * third) < 0) {
					fb = fc;
				}
			}	
		}
        
        cout.setf(ios::fixed);
        cout.precision(19);
        
		if (true == check) {
			cout << std::setprecision(19) << fc << endl;
		}
		else {
			cout << "NO SOLUTION" << endl;
		}

	}

	return 0;
}
