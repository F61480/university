#include <iostream>
#include <cstdio>
#include <cstdlib>

using namespace std;

const int moves[2][8] = {{1, 1, -1, -1, 2, -2, 2, -2}, {2, -2, 2, -2, 1, 1, -1, -1}};

bool MoveCheck(int x, int y , int n, int** matrix)
{
	if ((0 <= x && n > x && y >= 0 && n > y) && (0 == matrix[x][y])) {
		return true;
	}
	else {
		return false;
	}
}

bool KnightTour(int x, int y, int z, int n, int** matrix)
{
	matrix[x][y] = z;
    
	if (n*n == z) {
		return true;
	}
	else {
		for (int i = 0; i < 8; i++) {
			{
				if (true == (MoveCheck(x + moves[0][i], y + moves[1][i], n, matrix))) {
					if (true == (KnightTour(x + moves[0][i], y + moves[1][i], z + 1, n, matrix))) {
						return true;
					}
				}
			}
		}
		matrix[x][y] = 0;
	}
	return false;
}

void print(int** matrix, unsigned size) {
    for (unsigned i = 0 ; i < size; i++) {
			for (unsigned j = 0; j < size; j++){
				printf("%3u", matrix[i][j]);
			}
			cout << endl;
		}
}

int main() {
	unsigned entry = 0; 
    int XStart, YStart = 0;
	
    while (cin >> entry) {
        
        cin >> XStart >> YStart;
		
        XStart = entry - XStart;
		YStart = YStart - 1;
		
        int** matrix = new int*[entry];
        
		for(unsigned i = 0; i < entry; i++) {
			matrix[i] = new int[entry];
		}
        
		for(unsigned i = 0; i < entry; i++) {
			for(unsigned j = 0; j < entry; j++) {
				matrix[i][j] = 0;
			}
		}

		KnightTour(XStart, YStart, 1, entry, matrix);
        print(matrix, entry);
	}
	
	return 0;
}
