#include <iostream>
#include <cmath>
#include <vector>
#include <cstdlib>

using namespace std;

#define MAX 999

int main() {
	
    int results = 0;
    int seed = 0;
    int rdyRand = 0;
    unsigned long long entry;
	
	vector<bool> mybool(MAX+1);
    
	for (int i = 0; i < MAX; i++) {
        mybool[i] = true;
    }
    
	mybool[0] = mybool[1] = false;

	for (int i = 2; i < sqrt(MAX); i++) {
		if (true == mybool[i]) {
			for (int j = 2; i*j <= MAX; j++) {
				mybool[i*j] = false;
			}
		}
	}
	
    int counter = 0;
    
	while (cin >> seed >> entry) {
		results = 0;
		if (100 == counter) {
			break;
		}
        
		counter++;
		srand(seed);
        
		for (unsigned i = 0; i < entry; i++) {
			rdyRand = rand() % 1000;
			if (mybool[rdyRand]) {
				results++;
			}
		}
		cout << results << endl;
	}
	
	return 0;

}
