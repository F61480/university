#include <iostream>
#include <string>
#include <sstream>
#include <vector>

using namespace std;

int tmpEl = 0;

bool find(unsigned pos, vector<int> &myvec)
{
    int sum = 0;
    
	if (pos == myvec.size() - 1) {
		for (unsigned i = 0; i < myvec.size() - 1; i++) {
			sum += myvec[i];
		}

		if (tmpEl == sum) {
            return true;
        }
		else {
            return false;
        }
	}

	myvec[pos] *= -1;
    
	if (true == find(pos + 1, myvec)) {
        return true;
    }
    
	myvec[pos] *= -1;

	if (find(pos + 1, myvec)) {
        return true;
    }

	return false;
}

int main()
{
	

	string entry;
    
	while (getline(cin, entry)) {
        
		if (entry == "") {
            break;
        }

		stringstream data(entry);
        
        vector<int> myvec;
		int digits;
		

		while (data >> digits) {
			myvec.push_back(digits);
		}
        
		tmpEl = myvec[myvec.size() - 1];

		
		if (find(1, myvec) == false) {
			cout << endl;
		}
        
		else {
			cout << myvec[0];
            
			for (unsigned i = 1; i < myvec.size() - 1; i++) {
				if (myvec[i] > 0) {
					cout << "+";
				}
				cout << myvec[i];
			}
			cout << "=" << tmpEl;
			cout << endl;
		}
		
	}

	return 0;
}
