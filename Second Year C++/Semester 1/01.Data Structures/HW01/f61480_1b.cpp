//31 - OCT - 2016
//Tihomir Mladenov, F-61480
//Homework 01 - Exer. 1b

#include <iostream>
#include <string>
#include <cmath>
#include <cstdlib>

using namespace std;

void print(int arr[]) {
    for(int i = 0; i < 10000; i++) {
        cout << arr[i] << " ";
    }
    cout << endl;
}

int PrimeCheck(int tmp) {
    
    bool counter = false;
    
    for(int i = 2; i <= tmp / 2; i++) {
        if(tmp % i == 0) {
            counter = true;
            break;
        }
    }
    
    return counter;
}

int main() {
    
    int array[10000];
    srand(61480 % 100);
    
    int PrimeCount = 0;
    
    for(int i = 0; i < 10000; i++) {
        array[i] = rand()%1000;
        if(PrimeCheck(array[i]) == 0) {
            PrimeCount++;
        }
    }
    
    cout << PrimeCount << endl;
    
    return 0;
}
