//31 - OCT - 2016
//Tihomir Mladenov, F-61480
//Homework 01 - Exer. 1a

#include <iostream>
#include <string>
#include <cmath>

using namespace std;

int main() {

    while(cin) {
        int system;
        cin >> system;

        if(system == 2) {
            long entry;
            cin >> entry;

            long entry_size = entry;
            long digit_length = 1;
            while(entry_size /= 10) {
                digit_length++;
            }

            long result = 0;
            long tmp_int = 0;

            for(int i = 0; i < digit_length; i++) {
                tmp_int = entry % 10;
                entry /= 10;
                result += tmp_int * pow(2, i);
            }
            cout << result << endl;
        }

        else if(system == 8) {
            long entry;
            cin >> entry;

            long entry_size = entry;
            long digit_length = 1;
            while(entry_size /= 10) {
                digit_length++;
            }

            long result = 0;
            long tmp_int = 0;

            for(int i = 0; i < digit_length; i++) {
                tmp_int = entry % 10;
                entry /= 10;
                result += tmp_int * pow(8, i);
            }
            cout << result << endl;
        }

        else if(system == 16) {
            string entry;
            cin >> entry;

            long result = 0;
            int size = entry.length();

            for(int i = 0; i < entry.length(); i++) {
                if(entry[i] >= 48 && entry[i] <= 57) {
                    result += (entry[i] - 48) * pow(16, size - i - 1);
                }
                else if(entry[i] >= 65 && entry[i] <= 70) {
                    result += (entry[i] - 55) * pow(16, size - i - 1);
                }
                else if(entry[i] >= 97 && entry[i] <= 102) {
                    result += (entry[i] - 87) * pow(16, size - i - 1);
                }
            }

            cout << result << endl;
        }
        else {
            cout << "Enter 2, 8 or 16.\n";
        }
    }

    return 0;
}
