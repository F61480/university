﻿#include <iostream>
#include <string>
#include <vector>

using namespace std;

class Person {
private:
	string name;
public:
	Person(string n) {
		name = n;
	}

	string getName()
	{
		return name;
	}
};

int main()
{
	vector<Person*> p;
	int n;
	cin >> n;
	for (int i = 0; i<n; i++)
	{
		string s;
		cin >> s;
		p.push_back(new Person(s));
	}

	for (int i = 0; i < p.size(); i++)
	{
		cout << p[i]->getName() << endl;
	}

	system("pause");
	
	return 0;
}
