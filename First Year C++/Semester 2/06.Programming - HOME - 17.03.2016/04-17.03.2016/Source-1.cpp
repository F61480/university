﻿/*Да се дефинира указател, който да съдържа адрес в динамичната памет за записване на цяло число. Да се запише там число и да се отпечати. 
След това на същия указател да се присвои адрес в стека и да се отпечати стойността на указателя. Същото да се направи за (име на) масив 
вместо указател. Да се отпечатат елементите на масива, използвайки адресна аритметика.*/

#include <iostream>
#include <array>

using namespace std;

int main()
{
	cout << "Enter a digit in the stack.\n";
	int digit;
	cin >> digit;

	int *ndigit = new int(digit);
	cout << "This is transferred into the Heap with value "<< *ndigit << " at adress" << " " << ndigit << " ." << endl;

	int new_digit = *ndigit;

	cout << "Value transferred back to the Stack. Value is " << new_digit << " at address " << &new_digit << " . " << endl;

	int arr[3] = { 0, 30, 20 };
	
	cout << "The elements of the array are: " << endl;
	for (int i = 0; i < 3; i++)
	{
		cout << arr[i] << " ";
	}

	system("pause");

	return 0;
}