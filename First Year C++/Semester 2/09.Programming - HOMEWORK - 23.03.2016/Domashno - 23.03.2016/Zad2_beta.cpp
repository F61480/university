﻿#include <vector>
#include <iostream>
#include <string>

using namespace std;

class BaseClass {
public:
	BaseClass() { };

	// these pure virtual functions are re-defined in the derived classes
	virtual double F1() = 0;
	virtual double F2() = 0;
};

class DerivedClass1 : public BaseClass {
public:
	int a;
	int b;
	double c;
	double d1;
	DerivedClass1(int af, int bg) 
	{
		a = af;
		b = bg;
	}
	double F1()
	{
		return c = a * b;
	}
	double F2()
	{
		return d1 = a + b;
	}
};

class DerivedClass2 : public BaseClass {
public:
	int c;
	int d;
	double e;
	double f;
	DerivedClass2(int c, int d) { }
	double F1()
	{
		return e = c / d;
	}
	double F2()
	{
		return f = c - d;
	}
};

int main()
{
	vector<BaseClass*> baseClassObjects = vector<BaseClass*>();

	baseClassObjects.push_back(new DerivedClass1(1, 2));
	baseClassObjects.push_back(new DerivedClass2(2, 4));
	baseClassObjects.push_back(new DerivedClass2(3, 5));
	baseClassObjects.push_back(new DerivedClass1(7, 8));
	baseClassObjects.push_back(new DerivedClass1(2, 4));
	baseClassObjects.push_back(new DerivedClass2(6, 5));

	for (int i = 0; i < baseClassObjects.size(); i++)
	{
		BaseClass* ptr = baseClassObjects[i];
		cout << ptr->F1() << endl;
		cout << ptr->F2() << endl;
		cout << " " << endl;
	}

	system("pause");

	return 0;
}