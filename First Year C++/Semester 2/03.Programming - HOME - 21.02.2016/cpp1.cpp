#include <iostream>
#include <vector>
#include <fstream>

using namespace std;

void print(const vector<int> a)
{
	for (int i = 0; i < a.size(); i++)
	{
		cout << a[i] << " ";
	}
}

vector<int> calculation(const vector<int>& a, const vector<int>& b)
{
	vector<int> c;
	int i;
	for (i = 0; i < a.size() && b.size(); i++)
	{
		if (a[i] <= b[i])
		{
			c.push_back(a[i]);
			c.push_back(b[i]);
		}
		else if (b[i] <= a[i])
		{
			c.push_back(b[i]);
			c.push_back(a[i]);
		}
	}
	i++;
	for (; i < b.size(); i++)
	{
		c.push_back(b[i]);
	}
	for (; i < a.size(); i++)
	{
		c.push_back(a[i]);
	}
	return c;
}

int main()
{
	fstream in;
	in.open("input.txt");

	cout << "Enter N elements.\n";
	int n;
	in >> n;

	vector<int> vecn;
	for (int i = 0; i < n; i++)
	{
		int a;
		in >> a;
		vecn.push_back(a);
	}

	cout << "Enter M elements.\n";
	int m;
	in >> m;

	vector<int> vecm;
	for (int i = 0; i < m; i++)
	{
		int b;
		in >> b;
		vecm.push_back(b);
	}

	vector<int> result = calculation(vecn, vecm);
	print(result);
	in.close();
	system("pause");

	return 0;
}
