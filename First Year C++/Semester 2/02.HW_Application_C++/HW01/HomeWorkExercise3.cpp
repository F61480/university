#include <iostream>

using namespace std;

int main()
{
cout << "Enter an operation (a => +, s => -, m => *, p => /): " << endl;
char input;
cin >> input;

cout << "Enter operand 1: " << endl;
double x;
cin >> x;
cout << "Enter operand 2: " << endl;
double y;
cin >> y;

if (input == 'a')
{
cout << x << " + " << y << " is " << x+y << endl;
}
    else if (input == 's')
    {
    cout << x << " - " << y << " is " << x-y << endl;
    }
    else if (input == 'm')
    {
    cout << x << " * " << y << " is " << x*y << endl;
    }
    else if (input == 'p')
    {
    cout << x << " : " << y << " is " << x/y << endl;
    }

else
{
cout << "Invalid operation. Try again." << endl;
}

return 0;

}

