#include <iostream>
#include <iomanip>

using namespace std;

int main()
{
cout << "How many numbers would you like to use?" << endl;
int N;
cin >> N;
cout << "You have chosen to enter a set of " << N << ". Please enter the input." << endl;
double digit[N];

for(int add = 0; add < N; add++)
   {
       cin >> digit[add];
   }

cout << setfill('-');
cout << setw(15) << "  " << endl;

double smallest = digit[0];
    for(int add = 0; add < N; add++)
   {
       if(digit[add] < smallest)
           smallest = digit[add];
   }

   cout << "|min|" << setprecision(3) << smallest << "|" << endl;

double biggest = digit[0];
    for(int add = 0; add < N; add++)
    {
        if(digit[add] > biggest)
           biggest = digit[add];
    }

   cout << "|max|" << setprecision(3) << biggest << "|" << endl;

double summary;
double sum = 0;
    for(int add = 0; add < N; add++)
   {
       if(digit[add] > summary)
          sum += digit[add];
   }

   cout << "|sum|" << setprecision(3) << sum << "|" << endl;

double avarage;
double avg = 0;
    for(int add = 0; add < N; add++)
   {
       if(digit[add] > avarage)
          avg += digit[add];
   }

   cout << "|avg|" << setprecision(3) << avg/N << "|" << endl;

cout << setfill('-');
cout << setw(15) << "  " << endl;

return 0;
}
