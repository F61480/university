#include <iostream>
#include <string>
#include <sstream>

using namespace std;

int main()
{
    string entry;
    getline(cin, entry);
    
    //Takes entry's last two characters to determine AM or PM clock
    string PmOrAM = entry.substr(8, 9);

    if(PmOrAM == "AM") {
        string tmp2;
        
        //Takes the first to Digits into string
        for(int i = 0; i < 2; i++) {
            tmp2 += entry[i];
        }
        
        int tmp_clk;
        string tmp_clk_str;
        
        //Converts the first two digits to Int 32
        stringstream a(tmp2);
        a >> tmp_clk;
        
        
        if(tmp_clk == 12) {
            tmp_clk -= 12;
            
            //Converting the result back to String
            ostringstream b;
            b << tmp_clk;
            tmp_clk_str = b.str();
            
            //Conversion will remove the unnecessary 0, I am adding it back in due to the automated Test Case.
            tmp_clk_str += "0";
            
            //Replacing the first two caracters with the new value
            entry.replace(0, 2, tmp_clk_str);
            
            //Removing AM/PM values at the end due to 24H format
            entry.erase(8, 9);
            cout << entry << endl;
        } else {
            //Removing AM/PM values at the end due to 24H format
            entry.erase(8, 9);
            cout << entry << endl;
        }
    }

    if(PmOrAM == "PM") {
        string tmp2;
        for(int i = 0; i < 2; i++) {
            tmp2 += entry[i];
        }
        int tmp_clk;
        string tmp_clk_str;
        
        //Converts the first two digits to Int 32
        stringstream a(tmp2);
        a >> tmp_clk;

        if(tmp_clk == 24) {
            tmp_clk = 00;
            
            //Converting the result back to String
            ostringstream b;
            b << tmp_clk;
            tmp_clk_str = b.str();
            
            //Replacing the first two caracters with the new value
            entry.replace(0, 2, tmp_clk_str);
            
            //Removing AM/PM values at the end due to 24H format
            entry.erase(8, 9);
            cout << entry << endl;
        } else if(tmp_clk != 24 && tmp_clk != 12) {
            tmp_clk += 12;
            
            //Converting the result back to String
            ostringstream b;
            b << tmp_clk;
            tmp_clk_str = b.str();
            
            //Replacing the first two caracters with the new value
            entry.replace(0, 2, tmp_clk_str);
            
            //Removing AM/PM values at the end due to 24H format
            entry.erase(8, 9);
            cout << entry << endl;
        }
        else {
            //Removing AM/PM values at the end due to 24H format
            entry.erase(8, 9);
            cout << entry << endl;
        }
    }

    return 0;
}
