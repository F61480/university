#include <iostream>
#include <cstring>
#include <cctype>
#include <vector>
#include <algorithm>

using namespace std;

/*void print(const vector<bool> &myalpha) {
    for(int i = 0; i < myalpha.size(); i++) {
        cout << myalpha[i];
    }
}*/

int main()
{
    string entry;
    getline(cin, entry);
    vector<char> myvec;
    vector<bool> myalpha(26);
    
    myalpha[0] = true;
    for(int i = 1; i < myalpha.size(); i++) {
        myalpha[i] = false;
    }
    
    for(int i = 0; i < entry.size(); i++) {
        if(isalnum(entry[i])) {
            char tmp = entry[i];
            if(isupper(tmp)) {
                tmp = tolower(tmp);
                int charAscii = (int)tmp - 96;
                //cout << charAscii << endl;
                myalpha[charAscii] = true;
            }
            else {
                int charAscii = (int)tmp - 96;
                //cout << charAscii << endl;
                myalpha[charAscii] = true;
            }
            myvec.push_back(tmp);
        }
    }
    
    //print(myalpha);
    
    int checkAlpha = 0;
    
    for(int i = 0; i < myalpha.size(); i++) {
        if(myalpha[i] == true) {
            checkAlpha++;
        }
    }
    
    if(26 == checkAlpha) {
        cout << "pangram" << endl;
    }
    else {
        cout << "not pangram" << endl;
    }

    return 0;
}
