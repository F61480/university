﻿/***
FN:F61480
PID:2
GID:3
*/

#include <iostream>
#include <vector>
#include <string>
#include <cmath>

using namespace std;

class Worker
{
private:
public:
	Worker()
	{
	};
	virtual double salary() const = 0;
	virtual string getName() = 0;
};

class SalariedWorker : public Worker
{
private:
	string name;
	double sal;
public:
	SalariedWorker(string n, double num)
	{
		name = n;
		sal = num;
	}
	double salary() const
	{
		return sal;
	}
	string getName()
	{
		return name;
	}

};

class Consultant : public Worker
{
private:
	string name2;
	double sal2;
public:
	Consultant(string n2, double num2)
	{
		name2 = n2;
		sal2 = num2;
	}
	double salary() const
	{
		return sal2;
	}
	string getName()
	{
		return name2;
	}
};

int main()
{
	int n;
	cin >> n;

	vector<Worker *> BaseWorker = vector<Worker *>();
	BaseWorker.push_back(new SalariedWorker("no name", 0));

	int j = 1;

	while (j <= n)
	{
		string type;
		cin >> type;

		if (type == "s")
		{
			string n3;
			cin >> n3;
			double salar3;
			cin >> salar3;
			BaseWorker.push_back(new SalariedWorker(n3, salar3));
			j++;
		}
		else if (type == "c")
		{
			string n4;
			cin >> n4;
			double salar4;
			cin >> salar4;
			BaseWorker.push_back(new Consultant(n4, salar4));
			j++;
		}
	}

	int q;
	cin >> q;

	int j1 = 1;

	while (j1 <= q)
	{

		int index;
		cin >> index;

		if (index == 1)
		{

			double work_h;
			cin >> work_h;

			if (work_h < 40)
			{
				Worker * res = BaseWorker[index];
				cout << res->getName() << " worked " << work_h << " hours and should receive " << (work_h * res->salary()) / 2 << endl;
			}
			else if (work_h > 40)
			{
				Worker * res = BaseWorker[index];
				cout << res->getName() << " worked " << work_h << " hours and should receive " << (work_h * res->salary()) * 1.2 << endl;
			}
			else
			{
				Worker * res = BaseWorker[index];
				cout << res->getName() << " worked " << work_h << " hours and should receive " << work_h * res->salary() << endl;
			}
		}

		else if (index == 2)
		{
			double work_h;
			cin >> work_h;

			if (work_h <= 20)
			{
				Worker * res = BaseWorker[index];
				cout << res->getName() << " worked " << work_h << " hours and should receive " << work_h * res->salary() << endl;
			}
			else if (work_h > 20)
			{
				Worker * res = BaseWorker[index];
				cout << res->getName() << " worked " << work_h << " hours and should receive " << 20 * res->salary() << endl;
			}
		}
		j1++;
	}

	system("pause");

	return 0;
}