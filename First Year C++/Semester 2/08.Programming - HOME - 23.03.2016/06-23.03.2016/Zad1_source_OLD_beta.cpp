﻿#include <iostream>
#include <string>
#include <vector>

using namespace std;

class Person
{
private:
	string name;
	string birthdate;
public:
	Person();
	Person(string name, string birthdate);
	string get_name() const;
	void set_name(string new_name);
	string get_birthdate() const;
	void set_birthdate(string new_bd);
	void print() const;
};

Person::Person(string name, string birthdate)
{
	name = name;
	birthdate = birthdate;
}

string Person::get_name() const
{
	return name;
}

void Person::set_name(string new_name)
{
	name = new_name;
}

string Person::get_birthdate() const
{
	return birthdate;
}

void Person::set_birthdate(string new_bd)
{
	birthdate = new_bd;
}

void Person::print() const
{
	cout << "<" << name << ">" << " " << "(" << birthdate << ") " << endl;
}

class Student : public Person
{
private:
	string programme;
public:
	Student();
	Student(string name, string birthdate, string programme);
	string get_programme() const;
	void set_programme(string new_prog);
	void print() const;
};

Student::Student(string name, string birthdate, string programme) : Person(name, birthdate)
{
	programme = programme;
}

string Student::get_programme() const
{
	return programme;
}

void Student::set_programme(string new_prog)
{
	programme = new_prog;
}

void Student::print() const
{
	Person::print();
	cout << "studies " << "<" << programme << ">" << endl;
}

class Instructor : public Person
{
private:
	string sal;
public:
	Instructor();
	Instructor(string name, string birthdate, string sal);
	string get_sal() const;
	void set_sal(string new_sal);
	void print() const;
};

Instructor::Instructor(string name, string birhdate, string sal) : Person(name, birhdate)
{
	sal = sal;
}

string Instructor::get_sal() const
{
	return sal;
}

void Instructor::set_sal(string new_sal)
{
	sal = new_sal;
}

void Instructor::print() const
{
	Person::print();
	cout << "earns " << "<" << sal << ">" << endl;
}



int main()
{
	int n;
	cin >> n;

	vector<Student> s;
	vector<Instructor> i;

	int j = 0;

	while (j < n)
	{
		string info;
		cin >> info;
		if (info == "s")
		{
			string name;
			cin >> name;
			string birthdate;
			cin >> birthdate;
			string programme;
			cin >> programme;
			s.push_back(Student(name, birthdate, programme));
			j++;
		}
	}

	for (int i = 0; i < s.size(); i++)
	{
		s[i].print();
	}

	system("pause");
	return 0;
}