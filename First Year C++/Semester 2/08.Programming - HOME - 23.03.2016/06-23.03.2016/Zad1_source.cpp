﻿#include <iostream>
#include <string>
#include <vector>

using namespace std;

class Person 
{
private:
	string name;
	string birthd;

public:
	Person(string n, string bd)
	{
		name = n;
		birthd = bd;
	}

	string getName()
	{
		return name;
	}

	string getBD()
	{
		return birthd;
	}
};

class Student  : public Person
{
private:
	string programme;
public:
	Student(string n, string bd, string prog) : Person(n, bd)
	{
		programme = prog;
	}

	string getProg() const
	{
		return programme;
	}
};

void STprint(const vector<Student*> a)
{
	for (int i = 0; i<a.size(); i++)
	{
		std::cout << "<" << a[i]->getName() << ">" << " " << "(" << a[i]->getBD() << ")" << " " << " studies (" << a[i]->getProg() << ")" << " ";
	}
	std::cout << endl;
}

class Instructor : public Person
{
private:
	string salary;
public:
	Instructor(string n, string bd, string sal) : Person(n, bd)
	{
		salary = sal;
	}
	string getSal() const
	{
		return salary;
	}
};

void INSprint(const vector<Instructor*> b)
{
	for (int i = 0; i<b.size(); i++)
	{
		std::cout << "<" << b[i]->getName() << ">" << " " << "(" << b[i]->getBD() << ")" << " " << " earns (" << b[i]->getSal() << ")" << " ";
	}
	std::cout << endl;
}

int main()
{
	vector<Student*> stud;
	vector<Instructor*> ins;

	std::cout << "How many persons would you like to include?\n";

	int n;
	cin >> n;

	std::cout << "Enter - s - for student and - i - for instructor.\n";

	for (int i = 0; i<n; i++)
	{
		string info;
		cin >> info;
		if (info == "s")
		{
			std::cout << "Enter name.\n";
			string s;
			cin >> s;
			std::cout << "Enter birth date.\n";
			string d;
			cin >> d;
			std::cout << "Enter programme.\n";
			string e;
			cin >> e;
			stud.push_back(new Student(s, d, e));
		}
		else if (info == "i")
		{
			std::cout << "Enter name.\n";
			string s;
			cin >> s;
			std::cout << "Enter birth date.\n";
			string d;
			cin >> d;
			std::cout << "Enter salary.\n";
			string e;
			cin >> e;
			ins.push_back(new Instructor(s, d, e));
		}
		else
		{
			std::cout << "Choose between - s - or - i - only.\n";
			system("pause");
			return 1;
		}
	}

	bool more = true;
	while (more)
	{
		std::cout << "Enter - y - or - n - to continue.\n";
		string contin;
		cin >> contin;

		if (contin == "n")
		{
			more = false;
		}

		else if (contin == "y")
		{
			std::cout << "Enter row.\n";
			int row;
			cin >> row;

			std::cout << "Enter -s- for student or -i- for instructor.\n";
			string s_or_i;
			cin >> s_or_i;

			if (s_or_i == "s")
			{
				for (int i = 0; i < 1; i++)
				{
					int index = row;
					std::cout << "<" << stud[index]->getName() << ">" << " " << "(" << stud[index]->getBD() << ")" << " " << " studies (" << stud[index]->getProg() << ")" << " ";
				}
			}

			else if (s_or_i == "i")
			{
				for (int i = 0; i < 1; i++)
				{
					int index = row;
					std::cout << "<" << ins[index]->getName() << ">" << " " << "(" << ins[index]->getBD() << ")" << " " << " earns (" << ins[index]->getSal() << ")" << " ";
				}
			}
			std::cout << "Enter - y - or - n - to stop.\n";
		}
	}

	system("pause");

	return 0;
}