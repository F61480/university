﻿#include <iostream>
#include <string>
#include <vector>

using namespace std;

class Person {
private:
	string name;
	string birthd;

public:
	Person(string n, string bd)
	{
		name = n;
		birthd = bd;
	}

	string getName()
	{
		return name;
	}

	string getBD()
	{
		return birthd;
	}
};

void print(const vector<Person*> persons)
{
	for (int i = 0; i<persons.size(); i++)
	{
		cout << persons[i]->getName() << " " << persons[i]->getBD() << " ";
	}
}

int main()
{
	vector<Person*> p;
	int n;
	cin >> n;
	for (int i = 0; i<n; i++)
	{
		string s;
		cin >> s;
		string d;
		cin >> d;
		p.push_back(new Person(s, d));
	}
	print(p);

	system("pause");

	return 0;
}