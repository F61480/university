﻿/***
FN:F61480
PID:1
GID:2
*/

#include <iostream>
#include <string>
#include <vector>

using namespace std;

//implement class Student
//implement method printStudentInfo

class Student
{
private:
	string name;
	double avggrade;
public:
	Student(string n, double avg)
	{
		name = n;
		avggrade = avg;
	}
	virtual string getName() const
	{
		return name;
	}
	void setName(string new_name)
	{
		name = new_name;
	}
	virtual double getGradeAvarage() const
	{
		return avggrade;
	}
	void setGradeAverage(double new_avg)
	{
		avggrade = new_avg;
	}
	void print() const
	{
		cout << "My name is " << name << " and I have average grade of " << avggrade << "." << endl;
	}

};

void printStudentInfo(Student* a)
{
	cout << "The name of the student is " << a->getName() << "." << endl;
	cout << a->getName() << " has average grade of " << a->getGradeAvarage() << "." << endl;
}


int main()
{
	Student* john = new Student("John", 5.5);

	/*
	Should print:
	The name of the student is John.
	John has average grade of 5.5.
	*/
	printStudentInfo(john);

	//Should print "My name is John and I have average grade of 5.5."
	john->print();

	john->setName("Andrew");
	john->setGradeAverage(6.0);

	/*
	Should print:
	The name of the student is Andrew.
	Andrew has average grade of 6.
	*/
	printStudentInfo(john);

	//My name is Andrew and I have average grade of 6.
	john->print();

	delete john;

	return 0;
}