﻿/***
FN:F61480
PID:2
GID:2
*/

#include <vector>
#include <iostream>
#include <string>
#include <cmath>

using namespace std;

class BaseClass 
{
public:
	BaseClass() 
	{
	};
	virtual double area() = 0;
	virtual double perim() = 0;
};

class Triangular : public BaseClass 
{
private:
	double a;
	double b;
	double c;
	double ar;
	double semi_perim;
	double per;
public:
	Triangular(double s1, double s2, double s3)
	{
		a = s1;
		b = s2;
		c = s3;
	}
	double area()
	{
		semi_perim = (a + b + c) / 2;
		return ar = sqrt(semi_perim * (semi_perim - a)*(semi_perim - b)*(semi_perim - c));
	}
	double perim()
	{
		return per = a + b + c;
	}
};

class Circle : public BaseClass 
{
private:
	double e;
	double area_c;
	double per;
public:
	Circle(double c)
	{
		e = c;
	}
	double area()
	{
		return area_c = 3.14159 * pow(e, 2);
	}
	double perim()
	{
		return per = 2 * 3.14159 * e;
	}
};

int main()
{

	cout << "How many figures will you need?\n";
	int n;
	cin >> n;
	vector<BaseClass*> BaseClasses = vector<BaseClass*>();
	BaseClasses.push_back(new Triangular(0, 0, 0)); // Fills in the [0] element so that the user can use elements from [1] upward.

	int j = 1;

	while (j <= n)
	{
		cout << "Enter - t - for triangular or - c - for circle.\n";
		string option;
		cin >> option;
		if (option == "t")
		{
			cout << "Enter side 1, side 2 and side 3.\n";
			double side1, side2, side3;
			cin >> side1 >> side2 >> side3;
			BaseClasses.push_back(new Triangular(side1, side2, side3));
			j++;
		}
		else if (option == "c")
		{
			cout << "Enter the radius of the circle.\n";
			double data1;
			cin >> data1;
			BaseClasses.push_back(new Circle(data1));
			j++;
		}
		else
		{
			cout << "Choose between -t- and -c-. Run again.\n";
			return 1;
		}
	}

	bool more = true;

	while (more)
	{
		cout << "-a- for Area or -p- for perimeter.\n";
		string a_or_p;
		cin >> a_or_p;

		cout << "Enter the index (index >= 1).\n";
		int index;
		cin >> index;

		if (a_or_p == "a" && index > 0 && index <= j)
		{
			//for (int i = 0; i < BaseClasses.size() - index; i++)
			//{
				BaseClass* ptr = BaseClasses[index];
				cout << "The area is " << ptr->area() << endl;
				cout << " " << endl;
			//}
		}
		else if (a_or_p == "p" && index > 0 && index <= j)
		{
			//for (int i = 0; i < BaseClasses.size(); i++)
			//{
				BaseClass* ptr = BaseClasses[index];
				cout << "The perimeter is " << ptr->perim() << endl;
				cout << " " << endl;
			//}
		}
		else
		{
			cout << "Enter -a- or -p- or check your index. Run again.\n";
			return 1;
		}
	}

	return 0;
}