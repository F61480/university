#include <iostream>
#include <cassert>
#include <exception>

using namespace std;

template <typename T> class Array
{
private:
    T* elements;
    int size;
    static int element_count;
    void backup(const Array<T>& tmp)
    {
        size = tmp.size;
        elements = new int[size];
        for(int i = 0; i < size; i++) {
            elements[i] = tmp.elements[i];
        }
    }

public:
    Array<T>(int initial_size)
    {
        ++element_count;
        size = (initial_size > 0 ? initial_size : 10);
        elements = new int[size];

        for(int i = 0; i < size; i++)
            elements[i] = 0;
    }
    Array<T>(const Array& initial)
    {
        ++element_count;
        backup(initial);
    }
    ~Array()
    {
        --element_count;
        delete[] elements;
        elements = NULL;
    }

    int get_size() const
    {
        return size;
    }
    Array<T>& operator=(const Array<T>& right)
    {
        if(&right != this) {
            delete[] elements;
            backup(right);
        }

        return *this;
    }
    int& operator[](int index)
    {
        if(index < size) {
            return elements[index];
        }
        else {
            //cout << index;
            throw out_of_range(" is out of range");
        }
    }
    const int& operator[](int index) const
    {
        if(index < size) {
            return elements[index];
        }
        else {
            //cout << index;
            throw out_of_range(" is out of range");
        }
    }
    static int getElCount()
    {
        return element_count;
    }
    void swap(int* a, int* b)
    {
        int tmp = *a;
        *a = *b;
        *b = tmp;
    }

    void sort_array()
    {
        int i, j;
        for(i = 0; i < size - 1; i++) {
            for(j = 0; j < size - i - 1; j++) {
                if(elements[j] > elements[j + 1]) {
                    swap(&elements[j], &elements[j + 1]);
                }
            }
        }
    }
    friend ostream& operator<<(ostream& os, const Array<T>& a)
    {
        int i;
        for(i = 0; i < a.size; i++) {
            os << a.elements[i] << ' ';
        }

        return os;
    }
    friend istream& operator>>(istream& inp, Array<T>& a)
    {
        for(int i = 0; i < a.size; i++)
            inp >> a.elements[i];
        return inp;
    }
};
template <typename T> int Array<T>::element_count = 0;

int main()
{
    // do not change this
    Array<int> arr(5);
    arr[0] = 1;
    arr[1] = 2;

    cout << arr << endl;
    cout << "arr.size= " << arr.get_size() << endl;

    try {
        cout << arr[6] << endl;
    } catch(const out_of_range& e) {
        //remove 6 for showing the dynamic index
        cout << "6 is out of range" << endl;
    }

    arr.sort_array();
    cout << arr << endl;

    return 0;
}
