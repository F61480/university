/***
FN:F61480
PID:1
GID:1
*/

#include<fstream> 
#include<vector>
#include<string> 
#include<sstream> 
#include<iostream>
#include<cstdlib>

using namespace std;

int main(int argc, char * argv[])
{
	string program_name = string(argv[0]);

	string filename = string(argv[1]);
	ifstream infile;

	infile.open(filename.c_str());

	if (infile.fail())
	{
		cerr << "Error opining input file. Place the input file in the same folder as the .exe" << endl;
		exit(1);
	}

	double sum = 0;
	double avg = 0;
	int min = 0;
	int max = 0;
	int digit_count = 0;

	int digit;
	vector<double> results;

	while (infile)
	{
		infile >> digit;
		results.push_back(digit);
	}

	infile.close();

	// SUM
	for (int i = 0; i < results.size() - 1; i++)
	{
		sum += results[i];
		digit_count = i + 1;
	}
	cout << "SUM: " << sum << endl;

	// AVARAGE
	avg = sum / digit_count;
	cout << "AVG: " << avg << endl;

	// MIN value
	for (int i = 1; i < results.size() - 1; i++)
	{
		min = results[0];
		if (results[i] < min)
		{
			min = results[i];
		}
	}
	cout << "MIN: " << min << endl;

	// MAX value
	for (int i = 0; i < results.size() - 1; i++)
	{
		if (results[i] > max)
		{
			max = results[i];
		}
	}
	cout << "MAX: " << max << endl;

	return 0;
}
