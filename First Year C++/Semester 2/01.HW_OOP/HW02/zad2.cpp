/***
FN:F61480
PID:2
GID:1
*/

#include<iostream> 
#include<fstream> 
#include<string> 
#include<sstream> 
#include<vector>
#include<cstdlib>

using namespace std;

int main(int argc, char* argv[])
{
	vector<int> digit_vec;
	vector<int> results;
	int math = 0;
	int char_to_int = 0;

	if ((string(argv[3]) == "+" || string(argv[3]) == "-" || string(argv[3]) == "x"))
	{
		char * pend;
		char_to_int = strtol(argv[4], &pend, 10);

		ifstream infile;
		string filename = string(argv[1]);
		infile.open(filename.c_str());

		while (infile)
		{
			int digits;
			infile >> digits;
			digit_vec.push_back(digits);
		}

		infile.close();

		if (string(argv[3]) == "+")
		{
			for (unsigned i = 0; i < digit_vec.size() - 1; i++)
			{
				math = digit_vec[i] + char_to_int;
				results.push_back(math);
				math = 0;
			}

			ofstream outfile;
			outfile.open(string(argv[2]).c_str());

			for (unsigned i = 0; i < results.size(); i++)
			{
				outfile << results[i] << " ";
			}

			outfile.close();
		}
		else if (string(argv[3]) == "-")
		{
			for (unsigned i = 0; i < digit_vec.size() - 1; i++)
			{
				math = digit_vec[i] - char_to_int;
				results.push_back(math);
				math = 0;
			}
			ofstream outfile;
			outfile.open(string(argv[2]).c_str());

			for (unsigned i = 0; i < results.size(); i++)
			{
				outfile << results[i] << " ";
			}

			outfile.close();
		}
		else if (string(argv[3]) == "x")
		{
			for (unsigned i = 0; i < digit_vec.size() - 1; i++)
			{
				math = digit_vec[i] * char_to_int;
				results.push_back(math);
				math = 0;
			}

			ofstream outfile;
			outfile.open(string(argv[2]).c_str());

			for (unsigned i = 0; i < results.size(); i++)
			{
				outfile << results[i] << " ";
			}

			outfile.close();
		}
        
	}

	return 0;
}
