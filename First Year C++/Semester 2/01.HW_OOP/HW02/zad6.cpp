/***
FN:F61480
PID:6
GID:1
*/

#include <vector>
#include <iostream>
#include <algorithm>
#include <fstream>
#include <string>
#include <cstring>

using namespace std;

void clearchars(string& str, const char* removechars)
{
    for(unsigned int i = 0; i < strlen(removechars); ++i) {
        str.erase(remove(str.begin(), str.end(), removechars[i]), str.end());
    }
}

void findwords(string& filename, const string& search, string words)
{
    vector<string> text;
    int count = 0;

    ifstream infile;
    infile.open(filename.c_str());

    if(infile.fail()) {
        cerr << "Error opining input file. Place the input file in the same folder as the .exe" << endl;
        system("pause");
        exit(1);
    }

    while(!infile.eof()) {
        infile >> words;
        clearchars(words,
                   ",.;...()-?!"); // moje da se dobavqt i oshte simvoli, no dobavih tezi kato gledah uslovieto ni :)
        transform(words.begin(), words.end(), words.begin(), ::tolower);
        text.push_back(words);
    }
    infile.close();

    for(unsigned i = 0; i < text.size(); i++) {
        if(text[i] == search) {
            count++;
        }
    }
    text.clear();

    if(count > 0) {
        cout << filename << " " << count << endl;
    }
}

int main(int argc, char* argv[])
{
    if(argc > 5) {
        cout << "Enter 1 + 4 calling arguments.\n";
        return 1;
    }

    string words;

    string search = string(argv[1]);
    string second = string(argv[2]);
    string third = string(argv[3]);
    string fourth = string(argv[4]);

    findwords(second, search, words);
    findwords(third, search, words);
    findwords(fourth, search, words);

    return 0;
}
