/***
FN:F61480
PID:4
GID:1
*/

#include<iostream>

using namespace std;

int recursive(long long n1)
{
	if (n1 <= 3)
	{
		return 1;
	}
	else
	{
		return 3 * recursive(n1 - 1) - 2 * recursive(n1 - 2) + recursive(n1 - 3);
	}
}

int iterative(long long n2)
{
	if (n2 == 1 || n2 == 2 || n2 == 3)
	{
		return 1;
	}
	else
	{
		int a = 1; int b = 1; int c = 1;
		long long tmp;
		for (int i = 4; i <= n2; i++)
		{
			tmp = 3 * c - 2 * b + a;
			a = b;
			b = c;
			c = tmp;
		}
		return tmp;
	}
}

int main(int args, char * argv[])
{
	char * pend;
	long long n = 0;
	
	if (argv[1] != NULL)
	{
		n = atoi(argv[1]);
	}

	if (cin.fail())
	{
		cout << "Wrong input.\n";
		system("pause");
		return 1;
	}
	if (n <= 0)
	{
		cout << "Must be larger than 0.\n";
		system("pause");
		return 1;
	}

	recursive(n);
	iterative(n);

	cout << "recursive: " << recursive(n) << endl;
	cout << "iterative: " << iterative(n) << endl;

	system("pause");

	return 0;
}