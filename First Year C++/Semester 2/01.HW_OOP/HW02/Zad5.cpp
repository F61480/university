/***
FN:F61480
PID:5
GID:1
*/

#include<vector>
#include<iostream>
#include<algorithm>

using namespace std;

void print(const vector<int> & a)
{
	for (int i = 0; i < a.size(); i++)
	{
		cout << a[i] << " ";
	}
	cout << endl;
}

void BubbleSort(vector<int> &d)
{
	bool swapp = true;
	while (swapp)
	{
		swapp = false;
		for (size_t i = 0; i < d.size() - 1; i++)
		{
			if (d[i]>d[i + 1])
			{
				d[i] += d[i + 1];
				d[i + 1] = d[i] - d[i + 1];
				d[i] -= d[i + 1];
				swapp = true;
				print(d);
			}
		}
	}
	cout << endl;
}

void SelectionSort(vector<int> &num)
{
	int i;
	int j;

	for (j = 0; j < num.size() - 1; j++)
	{
		int minI = j;
		for (int i = j + 1; i < num.size(); i++)
		{
			if (num[i] < num[minI])
			{
				minI = i;
			}
		}
		print(num);
		if (minI != j)
		{
			swap(num[j], num[minI]);
		}
	}
}

int main()
{
	int n;
	cin >> n;

	vector<int> sort;
	vector<int> sort2;

	for (int i = 0; i < n; i++)
	{
		int entry;
		cin >> entry;
		sort.push_back(entry);
		sort2.push_back(entry);
	}
	cout << endl;

	BubbleSort(sort);
	SelectionSort(sort2);

	//print(sort);
	//print(sort2);

	system("pause");

	return 0;
}