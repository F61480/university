#ifndef PLAYER_H
#define PLAYER_H

using namespace std;

class Player{
public:
    Player();
    Player(string name, string date);
    string getName() const;
    string getDate() const;

protected:
    int age;
private:
    string name;
    string date;
};

Player::Player()
{
    this->name="default name";
    this->date="some date";
}

Player::Player(string name, string date)
{
    this->name=name;
    this->date=date;
}
Player::print()
{
    cout << this->name << " " << this->date << endl;
}

string Player::getName() const
{
    return this->name;
}

string Player::getDate() const
{
    return this->date;
}

#endif

