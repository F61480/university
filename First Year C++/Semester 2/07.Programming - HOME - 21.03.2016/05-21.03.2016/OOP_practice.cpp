﻿#include <iostream>
#include <string>

using namespace std;

class Person
{
private:
	string name;
	int age;
public:
	Person(string in_name, int in_age);
	void set_age(int new_age);
	void set_name(string new_name);
	string get_name() const;
	int get_age() const;
	void print() const;
};

Person::Person(string in_name, int in_age)
{
	name = in_name;
	age = in_age;
}

void Person::set_age(int new_age)
{
	age = new_age;
}

void Person::set_name(string new_name)
{
	name = new_name;
}

string Person::get_name() const
{
	return name;
}

int Person::get_age() const
{
	return age;
}

void Person::print() const
{
	cout << get_name() << " " << age << endl;
}

int main()
{
	string names;
	cin >> names;

	string bkp = names;

	int age;
	cin >> age;

	Person *tisho = new Person(names, age);

	tisho->print();

	cout << "Enter new age.\n";

	tisho->set_age(23);

	tisho->print();

	cout << "Press - y - to clear the dynamic memory! No memory leak!\n";
	string answer;
	cin >> answer;
	if (answer == "y")
	{
		delete(tisho);
	}

	system("pause");

	return 0;
}