﻿#include <iostream>
#include <array>

using namespace std;

int main()
{
	int arr[3];
	bool more = true;
	int index = 0;

	while (more)
	{
		if (index <= 2)
		{
			int enter;
			cin >> enter;
			arr[index] = enter;
			index++;
		}
		if (index > 2)
		{
			more = false;
		}
	}

	for (int i = 0; i < index; i++)
	{
		cout << arr[i] << endl;
	}

	system("pause");

	return 0;
}