﻿#include <iostream>
#include <array>
#include <cstring>

using namespace std;

void append(char ch[], int sizes, char t[])
{
	int i = strlen(ch);
	int j = 0;

	while (t[j] != '\0' && i < sizes)
	{
		ch[i] = t[j];
		i++;
		j++;
	}
	ch[i] = '\0';
}

int main()
{
	const int size = 13;
	char ch[size + 1] = "Hello, ";
	char t[] = "World.";

	append(ch, size, t);

	cout << "Appended result is " << ch << " With total symbol count of " << strlen(ch) << ".\n" << endl;

	system("pause");

	return 0;
}