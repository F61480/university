﻿#include <iostream>
#include <array>

using namespace std;

int maximum(const int arr_sal[], const int ind)
{
	int max = arr_sal[0];
	int i;
	for (i = 1; i < ind; i++)
	{
		if (arr_sal[i] > max)
		{
			max = arr_sal[i];
		}
	}
	return max;
}

int main()
{
	const int size = 5;

	int arr[size + 1];
	bool more = true;
	int index = 0;

	cout << "Enter the salaries (must be 6 in total).\n";

	while (more)
	{
		if (index <= 5)
		{
			int enter;
			cin >> enter;
			arr[index] = enter;
			index++;
		}
		if (index > 5)
		{
			more = false;
		}
	}

	int max_sal = maximum(arr, index);

	cout << "The highest salary is: " << max_sal << " BGN.\n";

	system("pause");

	return 0;
}