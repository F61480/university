#ifndef CHARACTER_H
#define CHARACTER_H
#include <string>
#include <iostream>


class Student {
private:
	std::string name;
	std::string specialty;
	int grade;
public:
	Student();
	Student(std::string name, std::string specialty, int grade);
	std::string getName() const;
	std::string getSpec() const;
	void Print() const;
	int getGrade() const;
};

#endif
