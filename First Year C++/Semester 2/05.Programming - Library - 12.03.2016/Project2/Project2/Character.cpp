#include "Character.h"
#include <string>

Student::Student() {
	name = "No name";
	specialty = "none";
	grade = 0;
}
Student::Student(std::string name, std::string specialty, int grade) {
	this->name = name;
	this->specialty = specialty;
	this->grade = grade;
}

/*Student::Student(std::string name, std::string specialty, int grade) : name(name), specialty(specialty), grade(grade);
{

}
*/
std::string Student::getName() const {
	return name;
}
std::string Student::getSpec() const {
	return specialty;
}
void Student::Print() const
{
	std::cout << name << " " << specialty << " " << grade << std::endl;
}
int Student::getGrade() const {
	return grade;
}