#include <iostream>
#include <algorithm>

using namespace std;

int main()
{
    cout << "Enter your name to see it reversed.\n";
    string name;
    cin >> name;

    reverse(name.begin(), name.end());

    cout << "Your reversed name is " << name << endl;

    return 0;
}
