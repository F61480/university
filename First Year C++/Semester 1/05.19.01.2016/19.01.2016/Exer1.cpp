#include <iostream>
#include <cmath>

using namespace std;

double total(double deposit, double interest_rate, double years)
{
    double result = deposit * pow(1 + interest_rate/100, years);
    return result;
}

int main()
{
    cout << "Enter: Deposit in USD, Interest rate percentage, Years of the investment.\n";
    double deposit, interest_rate, years;
    cin >> deposit >> interest_rate >> years;

    double newdeposit = total(deposit, interest_rate, years);

    cout << "In " << years << " years, the total of your account will be " << newdeposit << " USD.\n";

    return 0;
}
