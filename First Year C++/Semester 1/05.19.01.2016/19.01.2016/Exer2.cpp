#include <iostream>
#include <iomanip>

using namespace std;

double allGrade = 0;
double eachGrade = 0;
int gradeCount = 0;

int main()
{
    cout << "Enter your faculty number, first name and last name.\n";
    string facNum, first_names, last_name;
    cin >> facNum >> first_names >> last_name;

    cout << "Enter your grades, enter -1 when ready.\n";

    while(eachGrade != -1)
    {
        cin >> eachGrade;

        if(eachGrade != -1)
        {
            allGrade = eachGrade + allGrade;
            gradeCount++;
        }
    }

    double avarageGrade = allGrade / gradeCount;

    cout << setw(15) << "Faculty No." << setw(10) << "Grade" << endl;
    cout << setw(15) << facNum << setw(10) << avarageGrade;

    return 0;
}
