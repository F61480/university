#include <iostream>
#include <iomanip>

using namespace std;

int main()
{
cout << "Please enter your first name." << endl;
string firstname;
cin >> firstname;

cout << "The print of your name is: " << endl;
cout << setfill('=');
cout << setw(13) << "  " << endl;
cout << "=" << " " << firstname << " " << "=" << endl;
cout << setfill('=');
cout << setw(13) << "  " << endl;

return 0;
}
