#include <iostream>
#include <iomanip>

using namespace std;

int main()
{
string firstname;
cout << "Please enter your first name." << endl;
cin >> firstname;

string lastname;
cout << "Please enter your last name." << endl;
cin >> lastname;

string address;
cout << "What is your address?" << endl;
cin >> address;

cout << setfill('=');
cout << setw(15) << "  " << endl;

cout << ">" << " " << firstname << endl;
cout << ">" << " " << lastname << endl;
cout << ">" << " " << address << endl;

cout << setfill('=');
cout << setw(15) << "  " << endl;

return 0;
}
