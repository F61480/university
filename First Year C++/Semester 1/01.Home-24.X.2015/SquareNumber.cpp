#include <iostream>

using namespace std;

int main()

{
cout << "Please enter the number of which you need fo find the root:" << endl;
int a;
cin >> a;

if (a<=0)
    {
    cout << "Please run the program again and enter a digit bigger then 0." << endl;
    return 1;
    }
else if (a>0)
    {
    int b = a*a;
    int c = a*a*a;
    int d = a*a*a*a;
    int e = a*a*a*a*a;
    cout << "Your result is: "<< a << "^2=" << b << "; " << a << "^3=" << c << "; " << a << "^4=" << d << "; " << a << "^5=" << e << endl;
    return 0;
    }

return 0;
}
