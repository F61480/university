#include <iostream>

using namespace std;

int main()
{
cout << "Enter an operation (a => +, s => -, m => *, p => :): " << endl;
char input;
cin >> input;

cout << "Enter operand 1: " << endl;
int x;
cin >> x;
cout << "Enter operand 2: " << endl;
int y;
cin >> y;

    if (input == 'a')
    {
    cout << x << " + " << y << " is " << x+y << endl;
    }
    if (input == 's')
    {
    cout << x << " - " << y << " is " << x-y << endl;
    }
    if (input == 'm')
    {
    cout << x << " * " << y << " is " << x*y << endl;
    }
    if (input == 'p')
    {
    cout << x << " : " << y << " is " << x/y << endl;
    }

return 0;

}

