#include <iostream>

using namespace std;

int main()
{
double number = 0;
int count = 0;
double total = 0;

while(number != -1)
{
    cout << "Please enter a set of numbers, or enter -1 to finish.\n";
    cin >> number;
    if(cin.fail())
    {
    cout << "Try again. Enter a digit.\n";
    return 1;
    }

    if(number != -1)
    {
    total = number + total;
    count++;
    }
}

if (count > 0)
{
    cout << "The summary of all numbers is " << total << " and you have entered " << count << " numbers in total.\n";
}

return 0;
}
