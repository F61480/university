#include <iostream>

using namespace std;

int main()
{
double number, LOWEST = 0, HIGHEST = 1;

while(number != -1)
{
    cout << "Please enter a number, or enter -1 to finish.\n";
    cin >> number;

    if(number != -1 && number >= HIGHEST)
    HIGHEST = number;
    else if (number != -1)
    LOWEST = number;
}

cout << "The lowest number is " << LOWEST << " and the highest is " << HIGHEST << "." << endl;

return 0;
}
