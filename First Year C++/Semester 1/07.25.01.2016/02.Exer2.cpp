#include <iostream>
#include <vector>

using namespace std;

void reversing(vector<double> &number)
{
	for (int i = 0; i < number.size() / 2; i++)
	{
		int position = number[i];
		number[i] = number[number.size() - 1 - i];
		number[number.size() - 1 - i] = position;
	}
}

int main()
{
	cout << "Enter your vector, enter 0 afterward to stop.\n";
	vector<double> number;
	bool more = true;

	while (more)
	{
		double input;
		cin >> input;

		if (input == 0)
		{
			more = false;
		}
		else
			number.push_back(input);
	}

	reversing(number);

	cout << "The reversed vector is: " << endl;

	for (unsigned int i = 0; i < number.size(); i++)
	{
		cout << number[i] << ' ';
	}

	system("pause");

	return 0;
}