#include <iostream>
#include <vector>

using namespace std;

double calculate (vector<double> a, vector<double> b)
{
	double sum = 0;
	
	for (unsigned int i = 0; i < a.size() && i < b.size(); i++)
	{
		sum = sum + a[i] * b[i];
	}
	return sum;
}

int main()
{
	cout << "How many elements would you like to be included in the vector?\n";
	int counter;
	cin >> counter;

	cout << "Enter the elements of the first and then the second vector.\n";

	vector<double> myFirst;
	vector<double> mySecond;

	for (unsigned int i = 0; i < counter; i++)
	{
		double add;
		cin >> add;
		myFirst.push_back(add);
	}

	for (unsigned int i = 0; i < counter; i++)
	{
		double add1;
		cin >> add1;
		mySecond.push_back(add1);
	}

	double results = calculate(myFirst, mySecond);

	for (unsigned int i = 0; i < 1; i++)
	{
	cout << "The result of the addition of the first to second vector is: " << results << ".";
	}

	system("pause");

	return 0;
}