#include <iostream>
#include <vector>
#include <string>
#include <algorithm>

using namespace std;

void print(const vector<double> a)
{
	for (unsigned int i = 0; i < a.size(); i++)
	{
		cout << a[i] << " ";
	}
}

void removing(vector<double> &a, int pos)
{
	int i;
	for (i = pos; i < a.size() - 1; i++)
		a[i] = a[i + 1];
	a.pop_back();
}

int main()
{
	cout << "How many elements your vector will use?\n";
	int counter;
	cin >> counter;

	cout << "Please enter the elements of the vector.\n";

	vector<double> primary;

	for (int i = 0; i < counter; i++)
	{
		double add;
		cin >> add;
		primary.push_back(add);
	}

	cout << "Would you like to add new element to the vector - Yes/No/?\n";
	string answer;
	cin >> answer;

	if (answer == "Yes")
	{
		cout << "On which position would you like to include the new element?\n";
		int position;
		cin >> position;

		cout << "Enter the element.\n";
		int elements;
		cin >> elements;

		auto i = primary.emplace(primary.begin() + position, elements);
		cout << "The updated vector looks like: ";
		print(primary);
	}

	else if (answer == "No")
	{
		cout << "Would you like to remove any of the elements - Yes/No?\n";
		string answer2;
		cin >> answer2;

		if (answer2 == "Yes")
		{
			cout << "Which position would you like to remove?\n";
			int position1;
			cin >> position1;

			removing(primary, position1);
			cout << "Your answer is: \n";
			print(primary);
		}

		else if (answer2 == "No")
		{
			cout << "Would you like to shift-left the vector - Yes/No?\n";
			string answer3;
			cin >> answer3;

			if (answer3 == "Yes")
			{
				cout << "How many positions?\n";
				int position2;
				cin >> position2;
				
				rotate(primary.begin(), primary.begin() + position2, primary.end());
				cout << "Your result in rotating left the vector is:\n";
				print(primary);
			}
			else if (answer3 == "No")
			{
				cout << "Would you like to shift-right the vector - Yes/No?\n";
				string answer4;
				cin >> answer4;

				if (answer4 == "Yes")
				{
					cout << "How many positions?\n";
					int position3;
					cin >> position3;

					rotate(primary.rbegin(), primary.rbegin() + position3, primary.rend());
					cout << "Your result in rotating left the vector is:\n";
					print(primary);
				}

				else if (answer4 == "No")
				{
					cout << "You did not choose to add anything. The results is:\n";
					print(primary);
				}
			}
		}
		
	}

	system("pause");

	return 0;
}