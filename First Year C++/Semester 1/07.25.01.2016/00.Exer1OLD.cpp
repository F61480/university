#include <iostream>
#include <vector>

using namespace std;

vector<double>calculate (vector<double> a, vector<double> b)
{
	vector<double> calculation;
	for (unsigned int i = 0; i < a.size() && i < b.size(); i++)
	{
		calculation.push_back(a[0] * b[0] + a[1] * b[1] + a[2] * b[2]);
	}
	return calculation;
}

int main() 
{

	vector<double> myFirst;
	bool more = true;
	cout << "Enter the vector elemets, press 0 to stop.\n";

	while (more)
	{
		double entry;
		cin >> entry;

		if (entry == 0)
		{
			more = false;
		}
		else
			myFirst.push_back(entry);
	}

	vector<double> mySecond;
	bool againmore = true;
	cout << "Enter the vector elemets, press 0 to stop.\n";

	while (againmore)
	{
		double secondentry;
		cin >> secondentry;

		if (secondentry == 0)
		{
			againmore = false;
		}
		else
			mySecond.push_back(secondentry);
	}

	vector<double> results = calculate(myFirst, mySecond);

	for (unsigned int i = 0; i < 1; i++)
	{
		cout << "The result of the addition of the first to second vector is: " << results[i] << " ";
	}

	system("pause");

	return 0;
}