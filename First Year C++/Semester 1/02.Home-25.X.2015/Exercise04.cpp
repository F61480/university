#include <iostream>

using namespace std;

int main()

{
cout << "Enter product's price in BGN: " << endl;
double price;
cin >> price;

cout << "Enter the ammount of money you pay with in BGN: " << endl;
double payment;
cin >> payment;

double change = payment - price;
double cents = change * 100;

if (change<0)
    {
    cout << "The change is a negative number. Check your input.";
    }
else if (change>=0)
    {
    cout << "Your change is: " << change << " BGN" << " (" << cents << " st" << ")" << endl;
    }

return 0;
}
