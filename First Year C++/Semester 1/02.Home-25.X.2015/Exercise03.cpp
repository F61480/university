#include <iostream>

using namespace std;

//As per the description of the exercise, the input is set as 600.

int main()

{
cout << "Enter a number in meters:" << endl;
double meters;
cin >> meters;

double MetersToKilometers = meters / 1000;
double MetersToDecimeters = meters * 10;
double MetersToCentimeters = meters * 100;

cout << meters << " = " << MetersToKilometers << " km" << endl;
cout << meters << " = " << MetersToDecimeters << " dm" << endl;
cout << meters << " = " << MetersToCentimeters << " cm" << endl;

return 0;
}
