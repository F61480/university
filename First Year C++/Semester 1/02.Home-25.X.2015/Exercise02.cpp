#include <iostream>

//In the description of the exercise, the input is indicated as a=5 and b=6.

using namespace std;

int main()
{
cout << "Enter A:" << endl;
double a;
cin >> a;

cout << "Enter B:" << endl;
double b;
cin >> b;

int c = a + b;
int d = a - b;
int e = a * b;
double f = a / b;

cout << "a + b = " << c << endl;
cout << "a - b = " << d << endl;
cout << "a * b = " << e << endl;
cout << "a / b = " << f << endl;

return 0;
}
