#include <iostream>

using namespace std;

int main()
{
cout << "Enter tank size (liters): " << endl;
double tank;
cin >> tank;

cout << "Enter fuel efficiency (l/100km): " << endl;
double efficiency;
cin >> efficiency;

cout << "Enter price per liter in BGN: " << endl;
double price;
cin >> price;

double TravelDistance =  tank / efficiency;
double TravelDistance2 = TravelDistance * 100;

double PricePer100KM = price * efficiency;

cout << "We can travel " << TravelDistance2 << " km" << endl;
cout << "For every 100 km it will cost " << PricePer100KM << " BGN" << endl;

return 0;
}
