#include <iostream>
#include <cctype>
#include <string>

using namespace std;

bool is_number(const string &random)
{
    for(int i = 0; i < random.length(); i++)
    {
        if(!isdigit(random[i]))
            return false;
    }
    return true;
}

void check(string &random)
{
    if(is_number(random))
        random.append( "[OK]" );
    else
        random.append( "[FAIL]" );
}


int main()
{
    cout << "Enter your string. Num[OK]; Alphabet[FALSE]:\n";
    string random;
    cin >> random;

    check(random);

    cout << "The result is: " << random;

    return 0;
}
