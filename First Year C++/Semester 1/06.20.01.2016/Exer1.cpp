#include <iostream>

using namespace std;

double plusa(double left, double right)
{
    double r = left + right;
    return r;
}

double minusa(double left, double right)
{
    double r1 = left - right;
    return r1;
}

double devide(double left, double right)
{
    double r2 = left / right;
    return r2;
}

double times(double left, double right)
{
    double r3 = left * right;
    return r3;
}

int main()
{
    cout << "Enter your the two digits, one after the other: \n";
    double left, right;
    cin >> left >> right;
    if(cin.fail())
    {
        cout << "Enter the digit correctly.\n";
        return 1;
    }

    cout << "Select between the following operations (+, -, /, *): \n";
    string operation;
    cin >> operation;

    if(operation == "+")
    {
        double total1 = plusa(left, right);
        cout << "The result of operation " << operation << " is "<< total1;
        return 0;
    }
    else if(operation == "-")
    {
        double total2 = minusa(left, right);
        cout << "The result of operation " << operation << " is "<< total2;
        return 0;
    }
    else if(operation == "/")
    {
        double total3 = devide(left, right);
        cout << "The result of operation " << operation << " is "<< total3;
        return 0;
    }
    else if(operation == "*")
    {
        double total4 = times(left, right);
        cout << "The result of operation " << operation << " is "<< total4;
        return 0;
    }

    return 0;
}
