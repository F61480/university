#include <iostream>
#include <cctype>

using namespace std;

int main()
{
    cout << "Enter the first string that you want to check.\n";
    unsigned char randomstring;
    cin >> randomstring;

    cout << "Enter the second string that you want to check.\n";
    unsigned char randomstring2;
    cin >> randomstring2;

    cout << "The statement that the string " << randomstring << " is alphanumerical, is " << boolalpha << (bool)isalnum(randomstring) << '\n';
    cout << "The statement that the string " << randomstring2 << " is alphabetical, is " << boolalpha << (bool)isalpha(randomstring2) << '\n';

    return 0;
}
